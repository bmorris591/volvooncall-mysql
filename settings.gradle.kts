rootProject.name = "volvoncall-mysql"

enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

pluginManagement {
    repositories {
        mavenLocal()
        mavenCentral()
        gradlePluginPortal()
        maven("https://repo.spring.io/milestone")
    }
}

dependencyResolutionManagement {
    repositories {
        mavenLocal()
        mavenCentral()
        maven("https://repo.spring.io/milestone")
        maven("https://gitlab.com/api/v4/projects/26731120/packages/maven")
    }
}
