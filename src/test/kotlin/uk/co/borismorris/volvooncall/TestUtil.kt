@file:Suppress("DEPRECATION")

package uk.co.borismorris.volvooncall

import org.apache.commons.lang3.RandomStringUtils
import org.apache.commons.lang3.RandomUtils.nextDouble
import org.apache.commons.lang3.RandomUtils.nextInt
import org.apache.commons.lang3.RandomUtils.nextLong
import uk.co.borismorris.volvooncall.vocclient.BoundingBox
import uk.co.borismorris.volvooncall.vocclient.Country
import uk.co.borismorris.volvooncall.vocclient.Doors
import uk.co.borismorris.volvooncall.vocclient.Heater
import uk.co.borismorris.volvooncall.vocclient.HeaterAutoTimer
import uk.co.borismorris.volvooncall.vocclient.HvBattery
import uk.co.borismorris.volvooncall.vocclient.RouteDetails
import uk.co.borismorris.volvooncall.vocclient.SeatSelection
import uk.co.borismorris.volvooncall.vocclient.TheftAlarm
import uk.co.borismorris.volvooncall.vocclient.Trip
import uk.co.borismorris.volvooncall.vocclient.TripDetail
import uk.co.borismorris.volvooncall.vocclient.TripPosition
import uk.co.borismorris.volvooncall.vocclient.TripWaypoint
import uk.co.borismorris.volvooncall.vocclient.TripWaypointPosition
import uk.co.borismorris.volvooncall.vocclient.TyrePressure
import uk.co.borismorris.volvooncall.vocclient.Vehicle
import uk.co.borismorris.volvooncall.vocclient.VehicleAttributes
import uk.co.borismorris.volvooncall.vocclient.VehicleStatus
import uk.co.borismorris.volvooncall.vocclient.VehicleUrls
import uk.co.borismorris.volvooncall.vocclient.Windows
import java.net.URI
import java.time.Clock
import java.time.Instant
import java.time.LocalTime
import java.time.Year
import java.time.ZoneOffset
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit

private val localhost = URI("http://localhost").toURL()

fun ZonedDateTime.utcNoNano() = toInstant().truncatedTo(ChronoUnit.SECONDS)

fun mockVehicle(): Vehicle {
    val vehicleAttributes = VehicleAttributes(
        RandomStringUtils.randomAlphabetic(17),
        false,
        "",
        0,
        false,
        0,
        emptyList(),
        Country("GB"),
        "",
        false,
        "",
        0,
        "",
        "",
        0,
        0,
        false,
        emptyList(),
        "",
        false,
        false,
        false,
        false,
        0,
        Year.of(2018),
        0,
        false,
        false,
        randomString(),
        false,
        emptyList(),
        "",
        false,
        ZonedDateTime.ofInstant(Instant.EPOCH, ZoneOffset.UTC),
        ZonedDateTime.ofInstant(Instant.EPOCH, ZoneOffset.UTC),
        "",
        0,
        0,
        "",
        "",
        "",
        false,
        0,
        "",
        "",
        "",
        0,
    )
    val vehicleUrls = VehicleUrls(vehicleAttributes.vehicleIdentificationNumber, localhost, localhost, emptyList())
    val vehicle = Vehicle(vehicleUrls, vehicleAttributes)
    return vehicle
}

fun mockVehicleStatus() = VehicleStatus(
    0.0,
    ZonedDateTime.now(),
    0.0,
    ZonedDateTime.now(),
    "",
    ZonedDateTime.now(),
    listOf("bang", "pop", "wizz"),
    ZonedDateTime.now(),
    true,
    ZonedDateTime.now(),
    "Normal",
    ZonedDateTime.now(),
    0.0,
    ZonedDateTime.now(),
    Doors(false, false, false, false, false, false, ZonedDateTime.now()),
    false,
    ZonedDateTime.now(),
    0.0,
    0.0,
    ZonedDateTime.now(),
    ZonedDateTime.now(),
    Heater(
        SeatSelection(false, false, false, false, false),
        "Off",
        HeaterAutoTimer(false, LocalTime.MIDNIGHT),
        HeaterAutoTimer(false, LocalTime.NOON),
        ZonedDateTime.now(),
    ),
    mockHvBattery(),
    100,
    ZonedDateTime.now(),
    false,
    ZonedDateTime.now(),
    "Normal",
    ZonedDateTime.now(),
    "",
    ZonedDateTime.now(),
    TheftAlarm(1.0, 1.0, ZonedDateTime.now()),
    ZonedDateTime.now(),
    ZonedDateTime.now(),
    0.0,
    ZonedDateTime.now(),
    0.0,
    ZonedDateTime.now(),
    TyrePressure("Normal", "Normal", "Normal", "Normal", ZonedDateTime.now()),
    "Normal",
    ZonedDateTime.now(),
    Windows(false, false, false, false, ZonedDateTime.now()),
)

fun mockHvBattery() = HvBattery(
    0.0,
    ZonedDateTime.now(),
    "Normal",
    ZonedDateTime.now(),
    "Normal",
    "Normal",
    ZonedDateTime.now(),
    ZonedDateTime.now(),
    "",
    ZonedDateTime.now(),
    0.0,
    ZonedDateTime.now(),
    0.0,
    ZonedDateTime.now(),
)

fun mockRouteDetails() = RouteDetails(BoundingBox(51.519012, -0.141783, 51.501518, -0.143984), localhost, 10)

fun mockTrip(routeDetails: RouteDetails?, clock: Clock = Clock.systemUTC()) = mockTrip(routeDetails, (0..nextInt(2, 10)).asSequence().map { randomTripDetail(clock) }.toList())

fun mockTrip(routeDetails: RouteDetails?, tripDetails: List<TripDetail>): Trip {
    return Trip(
        randomString(),
        nextLong(),
        randomString(),
        routeDetails,
        localhost,
        tripDetails,
        randomString(),
    )
}

fun randomTripPos() = TripPosition(randomString(), "GB", 10.0, 10.0, randomString(), randomString(), randomString())

fun randomTripDetail(clock: Clock = Clock.systemUTC()) = TripDetail(
    nextDouble(),
    nextDouble(),
    nextDouble(),
    nextInt(),
    randomTripPos(),
    ZonedDateTime.now(clock),
    nextDouble(),
    nextInt(),
    randomTripPos(),
    ZonedDateTime.now(clock),
)

fun mockTrips(): List<Trip> = (5..10).asSequence().map { mockTrip(mockRouteDetails()) }.toList()

fun mockWaypoint(lat: Double, long: Double): TripWaypoint {
    val position = TripWaypointPosition(
        lat,
        long,
        100.30,
        123.0,
    )
    return TripWaypoint(
        ZonedDateTime.now(),
        1000,
        0.0,
        0.0,
        0.0,
        position,
    )
}

fun randomString(): String = RandomStringUtils.randomAlphabetic(10)
