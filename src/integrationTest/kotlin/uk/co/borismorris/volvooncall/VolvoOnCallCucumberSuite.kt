package uk.co.borismorris.volvooncall

import io.cucumber.core.options.Constants.GLUE_PROPERTY_NAME
import io.cucumber.core.options.Constants.PLUGIN_PROPERTY_NAME
import org.junit.platform.suite.api.ConfigurationParameter
import org.junit.platform.suite.api.ConfigurationParameters
import org.junit.platform.suite.api.IncludeEngines
import org.junit.platform.suite.api.SelectClasspathResource
import org.junit.platform.suite.api.Suite

@Suite
@IncludeEngines("cucumber")
@SelectClasspathResource("/uk/co/borismorris/volvooncall/features")
@ConfigurationParameters(
    ConfigurationParameter(key = GLUE_PROPERTY_NAME, value = "uk.co.borismorris.volvooncall"),
    ConfigurationParameter(key = PLUGIN_PROPERTY_NAME, value = "pretty"),
)
class VolvoOnCallCucumberSuite
