package uk.co.borismorris.volvooncall

import com.github.tomakehurst.wiremock.WireMockServer
import com.github.tomakehurst.wiremock.client.WireMock
import com.github.tomakehurst.wiremock.common.Slf4jNotifier
import com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig
import io.cucumber.java.After
import io.cucumber.java.Before
import io.cucumber.spring.CucumberContextConfiguration
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.annotation.DirtiesContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.DynamicPropertyRegistry
import org.springframework.test.context.DynamicPropertySource
import org.testcontainers.jdbc.ContainerDatabaseDriver

val wiremock = WireMockServer(
    wireMockConfig()
        .dynamicPort()
        .usingFilesUnderClasspath("uk/co/borismorris/volvooncall/vocclient/wiremock")
        .notifier(Slf4jNotifier(false))
        .templatingEnabled(true)
        .globalTemplating(true),
).also { it.start() }

@CucumberContextConfiguration
@SpringBootTest(
    classes = [VolvoOnCallApplication::class],
    properties = [
        "spring.datasource.url=jdbc:tc:mariadb:latest:///volvooncall?allowMultiQueries=true",
        "voc.username=username",
        "voc.password=password1234",
    ],
)
@DirtiesContext
@ActiveProfiles("test")
open class VolvoOnCallCucumberSpringGlue {
    companion object {
        @JvmStatic
        @DynamicPropertySource
        fun registerDysonProperties(registry: DynamicPropertyRegistry) {
            registry.add("voc.base-url") { "${wiremock.baseUrl()}/api" }
        }
    }
}

class VolvoOnCallWiremock {
    @Before
    fun bindWiremock() {
        wiremock.resetAll()
        WireMock.configureFor(wiremock.port())
    }

    @After
    fun killContainers() {
        ContainerDatabaseDriver.killContainers()
    }
}
