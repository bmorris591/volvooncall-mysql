package uk.co.borismorris.volvooncall.steps

import io.cucumber.java8.En
import org.assertj.core.api.Assertions.assertThat
import uk.co.borismorris.volvooncall.vocclient.CustomerAccount
import uk.co.borismorris.volvooncall.vocclient.RestclientVocClient

class CustomerAccountSteps(private val vocClient: RestclientVocClient) : En {
    lateinit var customerAccount: CustomerAccount

    init {
        When("I retrieve the customer account") {
            customerAccount = vocClient.customerAccount()
        }
        Then("the customer account is not null") {
            assertThat(customerAccount).isNotNull()
        }
    }
}
