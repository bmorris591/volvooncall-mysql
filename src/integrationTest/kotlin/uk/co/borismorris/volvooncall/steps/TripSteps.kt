package uk.co.borismorris.volvooncall.steps

import io.cucumber.java8.En
import org.assertj.core.api.Assertions.assertThat
import uk.co.borismorris.volvooncall.dao.TripRepository
import uk.co.borismorris.volvooncall.dao.TripWaypointRepository
import uk.co.borismorris.volvooncall.service.TripService
import uk.co.borismorris.volvooncall.vocclient.RestclientVocClient
import java.time.ZonedDateTime
import java.time.temporal.ChronoUnit

class TripSteps(
    private val vehicleSteps: VehicleSteps,
    private val vocClient: RestclientVocClient,
    private val tripService: TripService,
    private val tripRepository: TripRepository,
    private val waypointRepository: TripWaypointRepository,
) : En {
    init {
        When("I request that trips are persisted for {string}") { vin: String ->
            val persisted = tripService.saveTrips(vehicleSteps.vehicle(vin))
            assertThat(persisted).isGreaterThan(0)
        }
        Then("the count of trips is {int}") { expectedCount: Int ->
            val count = tripRepository.count()
            assertThat(count).isEqualTo(expectedCount.toLong())
        }
        Then("the tips persisted match those downloaded for {string}") { vin: String ->
            val trips = vocClient.vehicleTrips(vehicleSteps.vehicle(vin)).trips
            trips.forEach { expected ->
                val actual = tripRepository.load(expected.id)
                assertThat(actual)
                    .usingRecursiveComparison()
                    .ignoringFields("trip", "routeDetails.route")
                    .withEqualsForType({ l, r -> l.utcNoNano() == r.utcNoNano() }, ZonedDateTime::class.java)
                    .isEqualTo(expected)
            }
        }
        Then("the route persisted for {int} in {string} matches that downloaded") { tripId: Int, vin: String ->
            val trips = vocClient.vehicleTrips(vehicleSteps.vehicle(vin)).trips
            val expectedTrip = trips.first { it.id == tripId.toLong() }
            val expectedRoute = vocClient.tripRoute(expectedTrip).toList()
            val actualRoute = waypointRepository.load(tripId.toLong()).toList()

            assertThat(actualRoute)
                .usingRecursiveComparison()
                .withEqualsForType({ l, r -> l.utcNoNano() == r.utcNoNano() }, ZonedDateTime::class.java)
                .isEqualTo(expectedRoute)
        }
    }

    private fun ZonedDateTime.utcNoNano() = toInstant().truncatedTo(ChronoUnit.SECONDS)
}
