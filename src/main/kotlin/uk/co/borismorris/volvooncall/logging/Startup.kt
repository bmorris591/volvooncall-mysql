package uk.co.borismorris.volvooncall.logging

import io.github.oshai.kotlinlogging.KotlinLogging
import org.springframework.boot.context.event.ApplicationStartedEvent
import org.springframework.boot.info.GitProperties
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.event.EventListener
import uk.co.borismorris.volvooncall.config.VocConfig

private val logger = KotlinLogging.logger {}

class LogConfigOnStartup(private val context: ConfigurableApplicationContext) {
    @EventListener(ApplicationStartedEvent::class)
    fun onStartup() {
        context.getBean(VocConfig::class.java).also { logger.info { "vocConfig -> $it" } }
    }
}

class LogGitInfoOnStartup(val gitProperties: GitProperties?) {
    @EventListener(ApplicationStartedEvent::class)
    fun onStartup() {
        gitProperties?.apply {
            logger.info { "$branch@$commitId" }
        }
    }
}
