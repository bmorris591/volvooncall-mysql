package uk.co.borismorris.volvooncall.config

import java.net.URL

class VocConfig {
    lateinit var username: String
    lateinit var password: String
    lateinit var baseUrl: URL

    override fun toString() = "VocConfig(username='$username', password='XXX', baseUrl=$baseUrl)"
}
