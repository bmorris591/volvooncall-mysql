package uk.co.borismorris.volvooncall.service

import io.github.oshai.kotlinlogging.KotlinLogging
import org.apache.logging.log4j.CloseableThreadContext
import org.springframework.transaction.annotation.Transactional
import uk.co.borismorris.volvooncall.dao.TripRepository
import uk.co.borismorris.volvooncall.dao.TripWaypointRepository
import uk.co.borismorris.volvooncall.vocclient.RestclientVocClient
import uk.co.borismorris.volvooncall.vocclient.Trip
import uk.co.borismorris.volvooncall.vocclient.Vehicle

private val logger = KotlinLogging.logger {}

interface TripService {
    fun saveTrips(vehicle: Vehicle): Long
}

@Transactional
class DaoTripService(
    private val vocClient: RestclientVocClient,
    private val tripRepository: TripRepository,
    private val waypointRepository: TripWaypointRepository,
) : TripService {
    override fun saveTrips(vehicle: Vehicle) = vocClient.vehicleTrips(vehicle).trips.sumOf { trip ->
        CloseableThreadContext.put("trip", trip.id.toString()).use {
            if (!tripRepository.exists(trip)) {
                trip.saveTrip(vehicle)
            } else {
                0L
            }
        }
    }

    private fun Trip.saveTrip(vehicle: Vehicle): Long {
        logger.info { "Saving trip $this" }
        val updatedTrip = tripRepository.save(vehicle, this)
        logger.info { "Saved trip. Updated $updatedTrip row(s)" }
        val updatedRoute = saveRoute()
        logger.info { "Saved route. Updated $updatedRoute row(s)" }
        return updatedTrip + updatedRoute
    }

    private fun Trip.saveRoute(): Long = waypointRepository.save(this, vocClient.tripRoute(this))
}
