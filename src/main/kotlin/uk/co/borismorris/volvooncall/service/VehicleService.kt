package uk.co.borismorris.volvooncall.service

import io.github.oshai.kotlinlogging.KotlinLogging
import org.apache.logging.log4j.CloseableThreadContext
import org.springframework.transaction.annotation.Transactional
import uk.co.borismorris.volvooncall.dao.VehicleRepository
import uk.co.borismorris.volvooncall.dao.VehicleStatusRepository
import uk.co.borismorris.volvooncall.vocclient.CustomerAccount
import uk.co.borismorris.volvooncall.vocclient.RestclientVocClient
import uk.co.borismorris.volvooncall.vocclient.Vehicle
import java.time.Instant

private val logger = KotlinLogging.logger {}

interface VehicleService {
    fun vehicles(account: CustomerAccount): Sequence<Vehicle>

    fun saveStatus(vehicle: Vehicle): Int
}

@Transactional
class DaoVehicleService(
    private val vocClient: RestclientVocClient,
    private val vehicleRepository: VehicleRepository,
    private val vehicleStatusRepository: VehicleStatusRepository,
) : VehicleService {
    override fun vehicles(account: CustomerAccount) = vocClient.vehicles(account).asSequence().onEach { vehicle ->
        CloseableThreadContext.put("vehicle", vehicle.attributes.registrationNumber).use {
            if (!vehicleRepository.exists(vehicle)) {
                logger.info { "Saving Vehicle" }
                val updated = vehicleRepository.save(vehicle)
                logger.info { "Saved vehicle. Updated $updated row(s)" }
            }
        }
    }

    override fun saveStatus(vehicle: Vehicle) = vocClient.vehicleStatus(vehicle).let { status ->
        CloseableThreadContext.put("vehicle", vehicle.attributes.registrationNumber).use {
            logger.info { "Saving Vehicle status" }
            val (id, updated) = vehicleStatusRepository.save(vehicle, status, Instant.now())
            logger.info { "Saved vehicle status with id $id. Updated $updated row(s)" }
            id
        }
    }
}
