package uk.co.borismorris.volvooncall.worker

import io.github.oshai.kotlinlogging.KotlinLogging
import io.micrometer.observation.Observation
import io.micrometer.observation.ObservationRegistry
import jakarta.annotation.PostConstruct
import org.apache.logging.log4j.CloseableThreadContext
import org.springframework.scheduling.annotation.Scheduled
import uk.co.borismorris.volvooncall.service.TripService
import uk.co.borismorris.volvooncall.service.VehicleService
import uk.co.borismorris.volvooncall.vocclient.CustomerAccount
import uk.co.borismorris.volvooncall.vocclient.RestclientVocClient
import uk.co.borismorris.volvooncall.vocclient.Vehicle
import java.util.concurrent.TimeUnit
import java.util.concurrent.atomic.AtomicReference

private val logger = KotlinLogging.logger {}

class MonitorVolvoOnCall(
    private val observationRegistry: ObservationRegistry,
    private val vocClient: RestclientVocClient,
    private val vehicleService: VehicleService,
    private val tripService: TripService,
) {

    private val vehicles = AtomicReference<List<Vehicle>>()

    @PostConstruct
    fun initVehicles() {
        refreshVehicles()
    }

    @Scheduled(initialDelay = 1, fixedRate = 1, timeUnit = TimeUnit.DAYS)
    fun refreshVehicles() {
        Observation.createNotStarted("refresh-vehicles", observationRegistry)
            .observe {
                val account = vocClient.customerAccount()
                logger.info { "Retrieving vehicles for account $account" }
                vehicles.set(account.vehicles().toList())
            }
    }

    @Scheduled(initialDelay = 0, fixedRate = 1, timeUnit = TimeUnit.MINUTES)
    fun pollVehicleStatus() {
        Observation.createNotStarted("poll-vehicle-status", observationRegistry)
            .observe {
                vehicles.get()?.let { vehicles ->
                    vehicles.forEach { vehicle ->
                        CloseableThreadContext.put("vehicle", vehicle.attributes.registrationNumber).use {
                            pollStatus(vehicle)
                        }
                    }
                }
            }
    }

    @Scheduled(initialDelay = 0, fixedRate = 1, timeUnit = TimeUnit.HOURS)
    fun pollTrips() {
        Observation.createNotStarted("poll-trips", observationRegistry)
            .observe {
                vehicles.get()?.let { vehicles ->
                    vehicles.forEach { vehicle ->
                        CloseableThreadContext.put("vehicle", vehicle.attributes.registrationNumber).use {
                            pollTrips(vehicle)
                        }
                    }
                }
            }
    }

    private fun CustomerAccount.vehicles() = vehicleService.vehicles(this)

    private fun pollStatus(vehicle: Vehicle) {
        Observation.createNotStarted("poll-status", observationRegistry)
            .lowCardinalityKeyValue("vehicle.id", vehicle.id)
            .lowCardinalityKeyValue("vehicle.registration", vehicle.attributes.registrationNumber)
            .observe {
                logger.info { "Polling for status" }
                vehicle.saveStatus()
                logger.info { "Finished processing vehicle status" }
            }
    }

    private fun pollTrips(vehicle: Vehicle) {
        Observation.createNotStarted("poll-trips", observationRegistry)
            .lowCardinalityKeyValue("vehicle.id", vehicle.id)
            .lowCardinalityKeyValue("vehicle.registration", vehicle.attributes.registrationNumber)
            .observe {
                logger.info { "Polling for trips" }
                vehicle.saveTrips()
                logger.info { "Finished processing trips" }
            }
    }

    private fun Vehicle.saveTrips() = tripService.saveTrips(this@saveTrips)
    private fun Vehicle.saveStatus() = vehicleService.saveStatus(this@saveStatus)
}
