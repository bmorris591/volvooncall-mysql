@file:Suppress("UnsafeCallOnNullableType")

package uk.co.borismorris.volvooncall.vocclient

import com.fasterxml.jackson.annotation.JsonFormat
import com.fasterxml.jackson.annotation.JsonFormat.Shape.STRING
import com.fasterxml.jackson.annotation.JsonProperty
import org.springframework.http.MediaType
import org.springframework.web.client.RestClient
import org.springframework.web.client.body
import org.springframework.web.util.UriComponentsBuilder
import uk.co.borismorris.volvooncall.config.VocConfig
import java.net.URL
import java.time.LocalTime
import java.time.Year
import java.time.ZonedDateTime

private const val CUSTOMER_ACCOUNTS_PATH = "/customeraccounts"
private const val VEHICLE_TRIPS = "/vehicles/{vehicleId}/trips"
private const val DATE_FORMAT_PATTERN = "yyyy-MM-dd'T'HH:mm:ssZ"
private const val WAYPOINT_PAGE_SIZE = 1000

class RestclientVocClient(config: VocConfig, restClientBuilder: RestClient.Builder) {
    private val restClient = restClientBuilder
        .defaultHeaders {
            it.setBasicAuth(config.username, config.password)
            it.contentType = MediaType.APPLICATION_JSON
            it.accept = listOf(MediaType.APPLICATION_JSON)
        }
        .defaultHeader("X-Device-Id", "Device")
        .defaultHeader("X-OS-Type", "Android")
        .defaultHeader("X-Originator-Type", "App")
        .defaultHeader("X-OS-Version", "22")
        .baseUrl(config.baseUrl.toString())
        .build()

    fun customerAccount(): CustomerAccount = restClient.get()
        .uri(CUSTOMER_ACCOUNTS_PATH)
        .retrieve()
        .body()!!

    fun vehicles(account: CustomerAccount) = account.vehicleRelations
        .map { getVehicle(it) }

    private fun getVehicle(relationUrl: URL): Vehicle {
        val relation = getVehicleAccountRelation(relationUrl)
        val urls = getVehicleUrls(relation.vehicleUrl)
        val attributes = getVehicleAttributes(urls.attributesUrl)
        return Vehicle(urls, attributes)
    }

    private fun getVehicleAccountRelation(relationUrl: URL): VehicleAccountRelation = restClient.get()
        .uri(relationUrl.toURI())
        .retrieve()
        .body()!!

    private fun getVehicleUrls(vehicleUrl: URL): VehicleUrls = restClient.get()
        .uri(vehicleUrl.toURI())
        .retrieve()
        .body()!!

    private fun getVehicleAttributes(vehicleAttributesUrl: URL): VehicleAttributes = restClient.get()
        .uri(vehicleAttributesUrl.toURI())
        .retrieve()
        .body()!!

    fun vehicleStatus(vehicle: Vehicle): VehicleStatus = restClient.get()
        .uri(vehicle.urls.statusUrl.toURI())
        .retrieve()
        .body()!!

    fun vehicleTrips(vehicle: Vehicle): Trips = restClient.get()
        .uri { it.path(VEHICLE_TRIPS).build(vehicle.urls.vehicleId) }
        .retrieve()
        .body()!!

    fun tripRoute(trip: Trip): Sequence<TripWaypoint> = (0 until trip.routeDetails.pages())
        .asSequence()
        .map { getWaypoints(trip, it) }
        .takeWhile { it.waypoints.isNotEmpty() }
        .flatMap { it.waypoints.asSequence() }

    private fun RouteDetails?.pages() = this?.let { (it.totalWaypoints + WAYPOINT_PAGE_SIZE - 1) / WAYPOINT_PAGE_SIZE } ?: Int.MAX_VALUE

    private fun getWaypoints(trip: Trip, page: Int): TripRoute {
        val routeUri = trip.routeDetails?.let { UriComponentsBuilder.fromUri(it.route.toURI()) } ?: UriComponentsBuilder.fromUri(trip.trip.toURI()).pathSegment("route")

        val uri = routeUri
            .queryParam("page", page)
            .queryParam("page_size", WAYPOINT_PAGE_SIZE)
            .build()
            .toUriString()
        return restClient.get().uri(uri).retrieve().body()!!
    }
}

@JvmRecord
data class Vehicle(val urls: VehicleUrls, val attributes: VehicleAttributes) {
    val id: String get() = urls.vehicleId
}

@JvmRecord
data class VehicleAttributes(
    @JsonProperty("vin") val vehicleIdentificationNumber: String,
    @JsonProperty("assistanceCallSupported") val assistanceCallSupported: Boolean,
    @JsonProperty("bCallAssistanceNumber") val bCallAssistanceNumber: String,
    @JsonProperty("carLocatorDistance") val carLocatorDistance: Int,
    @JsonProperty("carLocatorSupported") val carLocatorSupported: Boolean,
    @JsonProperty("climatizationCalendarMaxTimers") val climatizationCalendarMaxTimers: Int,
    @JsonProperty("climatizationCalendarVersionsSupported") val climatizationCalendarVersionsSupported: List<String>,
    @JsonProperty("country") val country: Country,
    @JsonProperty("engineCode") val engineCode: String,
    @JsonProperty("engineStartSupported") val engineStartSupported: Boolean,
    @JsonProperty("exteriorCode") val exteriorCode: String,
    @JsonProperty("fuelTankVolume") val fuelTankVolume: Int,
    @JsonProperty("fuelType") val fuelType: String,
    @JsonProperty("gearboxCode") val gearboxCode: String,
    @JsonProperty("grossWeight") val grossWeight: Int,
    @JsonProperty("honkAndBlinkDistance") val honkAndBlinkDistance: Int,
    @JsonProperty("honkAndBlinkSupported") val honkAndBlinkSupported: Boolean,
    @JsonProperty("honkAndBlinkVersionsSupported") val honkAndBlinkVersionsSupported: List<String>,
    @JsonProperty("interiorCode") val interiorCode: String,
    @JsonProperty("journalLogEnabled") val journalLogEnabled: Boolean,
    @JsonProperty("journalLogSupported") val journalLogSupported: Boolean,
    @JsonProperty("highVoltageBatterySupported") val highVoltageBatterySupported: Boolean,
    @JsonProperty("lockSupported") val lockSupported: Boolean,
    @JsonProperty("maxActiveDelayChargingLocations") val maxActiveDelayChargingLocations: Int,
    @JsonProperty("modelYear") val modelYear: Year,
    @JsonProperty("numberOfDoors") val numberOfDoors: Int,
    @JsonProperty("overrideDelayChargingSupported") val overrideDelayChargingSupported: Boolean,
    @JsonProperty("preclimatizationSupported") val preclimatizationSupported: Boolean,
    @JsonProperty("registrationNumber") val registrationNumber: String,
    @JsonProperty("remoteHeaterSupported") val remoteHeaterSupported: Boolean,
    @JsonProperty("sendPOIToVehicleVersionsSupported") val sendPOIToVehicleVersionsSupported: List<String>,
    @JsonProperty("serverVersion") val serverVersion: String,
    @JsonProperty("status.parkedIndoor.supported") val statusParkedIndoorSupported: Boolean,
    @JsonProperty("subscriptionEndDate")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val subscriptionEndDate: ZonedDateTime,
    @JsonProperty("subscriptionStartDate")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val subscriptionStartDate: ZonedDateTime,
    @JsonProperty("subscriptionType") val subscriptionType: String,
    @JsonProperty("timeFullyAccessible") val timeFullyAccessible: Int,
    @JsonProperty("timePartiallyAccessible") val timePartiallyAccessible: Int,
    @JsonProperty("tyreDimensionCode") val tyreDimensionCode: String,
    @JsonProperty("tyreInflationPressureHeavyCode") val tyreInflationPressureHeavyCode: String,
    @JsonProperty("tyreInflationPressureLightCode") val tyreInflationPressureLightCode: String,
    @JsonProperty("unlockSupported") val unlockSupported: Boolean,
    @JsonProperty("unlockTimeFrame") val unlockTimeFrame: Int,
    @JsonProperty("vehiclePlatform") val vehiclePlatform: String,
    @JsonProperty("vehicleType") val vehicleType: String,
    @JsonProperty("vehicleTypeCode") val vehicleTypeCode: String,
    @JsonProperty("verificationTimeFrame") val verificationTimeFrame: Int,
)

@JvmRecord
data class Country(
    @JsonProperty("iso2") val iso2: String,
)

@JvmRecord
data class VehicleUrls(
    @JsonProperty("vehicleId") val vehicleId: String,
    @JsonProperty("attributes") val attributesUrl: URL,
    @JsonProperty("status") val statusUrl: URL,
    @JsonProperty("vehicleAccountRelations") val accountRelations: List<URL>,
)

@JvmRecord
data class VehicleAccountRelation(
    @JsonProperty("accountId") val accountId: String,
    @JsonProperty("account") val accountUrl: URL,
    @JsonProperty("vehicle") val vehicleUrl: URL,
    @JsonProperty("accountVehicleRelation") val accountVehicleRelationUrl: URL,
    @JsonProperty("vehicleId") val vehicleId: String,
    @JsonProperty("username") val username: String,
    @JsonProperty("status") val status: String,
    @JsonProperty("customerVehicleRelationId") val customerVehicleRelationId: String,
)

@JvmRecord
data class CustomerAccount(
    @JsonProperty("username") val username: String,
    @JsonProperty("firstName") val firstName: String,
    @JsonProperty("lastName") val lastName: String,
    @JsonProperty("accountId") val accountId: String,
    @JsonProperty("account") val accountUrl: URL,
    @JsonProperty("accountVehicleRelations") val vehicleRelations: List<URL>,
)

@JvmRecord
data class VehicleStatus(
    @JsonProperty("averageFuelConsumption") val averageFuelConsumption: Double,
    @JsonProperty("averageFuelConsumptionTimestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val averageFuelConsumptionTimestamp: ZonedDateTime,
    @JsonProperty("averageSpeed") val averageSpeed: Double,
    @JsonProperty("averageSpeedTimestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val averageSpeedTimestamp: ZonedDateTime,
    @JsonProperty("brakeFluid") val brakeFluid: String,
    @JsonProperty("brakeFluidTimestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val brakeFluidTimestamp: ZonedDateTime,
    @JsonProperty("bulbFailures") val bulbFailures: List<String>,
    @JsonProperty("bulbFailuresTimestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val bulbFailuresTimestamp: ZonedDateTime,
    @JsonProperty("carLocked") val carLocked: Boolean,
    @JsonProperty("carLockedTimestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val carLockedTimestamp: ZonedDateTime,
    @JsonProperty("connectionStatus") val connectionStatus: String,
    @JsonProperty("connectionStatusTimestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val connectionStatusTimestamp: ZonedDateTime,
    @JsonProperty("distanceToEmpty") val distanceToEmpty: Double,
    @JsonProperty("distanceToEmptyTimestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val distanceToEmptyTimestamp: ZonedDateTime,
    @JsonProperty("doors") val doors: Doors,
    @JsonProperty("engineRunning") val engineRunning: Boolean,
    @JsonProperty("engineRunningTimestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val engineRunningTimestamp: ZonedDateTime,
    @JsonProperty("fuelAmount") val fuelAmount: Double,
    @JsonProperty("fuelAmountLevel") val fuelAmountLevel: Double,
    @JsonProperty("fuelAmountLevelTimestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val fuelAmountLevelTimestamp: ZonedDateTime,
    @JsonProperty("fuelAmountTimestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val fuelAmountTimestamp: ZonedDateTime,
    @JsonProperty("heater") val heater: Heater,
    @JsonProperty("hvBattery") val hvBattery: HvBattery,
    @JsonProperty("odometer") val odometer: Int,
    @JsonProperty("odometerTimestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val odometerTimestamp: ZonedDateTime,
    @JsonProperty("parkedIndoor") val parkedIndoor: Boolean,
    @JsonProperty("parkedIndoorTimestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val parkedIndoorTimestamp: ZonedDateTime,
    @JsonProperty("remoteClimatizationStatus") val remoteClimatizationStatus: String,
    @JsonProperty("remoteClimatizationStatusTimestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val remoteClimatizationStatusTimestamp: ZonedDateTime,
    @JsonProperty("serviceWarningStatus") val serviceWarningStatus: String,
    @JsonProperty("serviceWarningStatusTimestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val serviceWarningStatusTimestamp: ZonedDateTime,
    @JsonProperty("theftAlarm") val theftAlarm: TheftAlarm?,
    @JsonProperty("timeFullyAccessibleUntil")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val timeFullyAccessibleUntil: ZonedDateTime,
    @JsonProperty("timePartiallyAccessibleUntil")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val timePartiallyAccessibleUntil: ZonedDateTime,
    @JsonProperty("tripMeter1") val tripMeter1: Double,
    @JsonProperty("tripMeter1Timestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val tripMeter1Timestamp: ZonedDateTime,
    @JsonProperty("tripMeter2") val tripMeter2: Double,
    @JsonProperty("tripMeter2Timestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val tripMeter2Timestamp: ZonedDateTime,
    @JsonProperty("tyrePressure") val tyrePressure: TyrePressure,
    @JsonProperty("washerFluidLevel") val washerFluidLevel: String,
    @JsonProperty("washerFluidLevelTimestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val washerFluidLevelTimestamp: ZonedDateTime,
    @JsonProperty("windows") val windows: Windows,
)

@JvmRecord
data class Doors(
    @JsonProperty("frontLeftDoorOpen") val frontLeftDoorOpen: Boolean,
    @JsonProperty("frontRightDoorOpen") val frontRightDoorOpen: Boolean,
    @JsonProperty("hoodOpen") val hoodOpen: Boolean,
    @JsonProperty("rearLeftDoorOpen") val rearLeftDoorOpen: Boolean,
    @JsonProperty("rearRightDoorOpen") val rearRightDoorOpen: Boolean,
    @JsonProperty("tailgateOpen") val tailgateOpen: Boolean,
    @JsonProperty("timestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val timestamp: ZonedDateTime,
)

@JvmRecord
data class Heater(
    @JsonProperty("seatSelection") val seatSelection: SeatSelection,
    @JsonProperty("status") val status: String,
    @JsonProperty("timer1") val timer1: HeaterAutoTimer,
    @JsonProperty("timer2") val timer2: HeaterAutoTimer,
    @JsonProperty("timestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val timestamp: ZonedDateTime,
)

@JvmRecord
data class HvBattery(
    @JsonProperty("distanceToHVBatteryEmpty") val distanceToHVBatteryEmpty: Double,
    @JsonProperty("distanceToHVBatteryEmptyTimestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val distanceToHVBatteryEmptyTimestamp: ZonedDateTime,
    @JsonProperty("hvBatteryChargeModeStatus") val hvBatteryChargeModeStatus: String,
    @JsonProperty("hvBatteryChargeModeStatusTimestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val hvBatteryChargeModeStatusTimestamp: ZonedDateTime,
    @JsonProperty("hvBatteryChargeStatus") val hvBatteryChargeStatus: String,
    @JsonProperty("hvBatteryChargeStatusDerived") val hvBatteryChargeStatusDerived: String?,
    @JsonProperty("hvBatteryChargeStatusDerivedTimestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val hvBatteryChargeStatusDerivedTimestamp: ZonedDateTime?,
    @JsonProperty("hvBatteryChargeStatusTimestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val hvBatteryChargeStatusTimestamp: ZonedDateTime,
    @JsonProperty("hvBatteryChargeWarning") val hvBatteryChargeWarning: String,
    @JsonProperty("hvBatteryChargeWarningTimestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val hvBatteryChargeWarningTimestamp: ZonedDateTime,
    @JsonProperty("hvBatteryLevel") val hvBatteryLevel: Double,
    @JsonProperty("hvBatteryLevelTimestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val hvBatteryLevelTimestamp: ZonedDateTime,
    @JsonProperty("timeToHVBatteryFullyCharged") val timeToHVBatteryFullyCharged: Double,
    @JsonProperty("timeToHVBatteryFullyChargedTimestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val timeToHVBatteryFullyChargedTimestamp: ZonedDateTime,
)

@JvmRecord
data class TyrePressure(
    @JsonProperty("frontLeftTyrePressure") val frontLeftTyrePressure: String,
    @JsonProperty("frontRightTyrePressure") val frontRightTyrePressure: String,
    @JsonProperty("rearLeftTyrePressure") val rearLeftTyrePressure: String,
    @JsonProperty("rearRightTyrePressure") val rearRightTyrePressure: String,
    @JsonProperty("timestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val timestamp: ZonedDateTime,
)

@JvmRecord
data class TheftAlarm(
    @JsonProperty("longitude") val latitude: Double,
    @JsonProperty("latitude") val longitude: Double,
    @JsonProperty("timestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val timestamp: ZonedDateTime,
)

@JvmRecord
data class Windows(
    @JsonProperty("frontLeftWindowOpen") val frontLeftWindowOpen: Boolean,
    @JsonProperty("frontRightWindowOpen") val frontRightWindowOpen: Boolean,
    @JsonProperty("rearLeftWindowOpen") val rearLeftWindowOpen: Boolean,
    @JsonProperty("rearRightWindowOpen") val rearRightWindowOpen: Boolean,
    @JsonProperty("timestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val timestamp: ZonedDateTime,
)

@JvmRecord
data class SeatSelection(
    @JsonProperty("frontDriverSide") val frontDriverSide: Boolean,
    @JsonProperty("frontPassengerSide") val frontPassengerSide: Boolean,
    @JsonProperty("rearDriverSide") val rearDriverSide: Boolean,
    @JsonProperty("rearMid") val rearMid: Boolean,
    @JsonProperty("rearPassengerSide") val rearPassengerSide: Boolean,
)

@JvmRecord
data class HeaterAutoTimer(
    @JsonProperty("state") val state: Boolean,
    @JsonProperty("time")
    @JsonFormat(shape = STRING, pattern = "HH:mm")
    val time: LocalTime,
)

@JvmRecord
data class Trips(
    @JsonProperty("trips") val trips: List<Trip>,
)

@JvmRecord
data class Trip(
    @JsonProperty("category") val category: String,
    @JsonProperty("id") val id: Long,
    @JsonProperty("name") val name: String?,
    @JsonProperty("routeDetails") val routeDetails: RouteDetails?,
    @JsonProperty("trip") val trip: URL,
    @JsonProperty("tripDetails") val tripDetails: List<TripDetail>,
    @JsonProperty("userNotes") val userNotes: String?,
)

@JvmRecord
data class RouteDetails(
    @JsonProperty("boundingBox") val boundingBox: BoundingBox,
    @JsonProperty("route") val route: URL,
    @JsonProperty("totalWaypoints") val totalWaypoints: Int,
)

@JvmRecord
data class TripDetail(
    @JsonProperty("distance") val distance: Double,
    @JsonProperty("electricalConsumption") val electricalConsumption: Double,
    @JsonProperty("electricalRegeneration") val electricalRegeneration: Double,
    @JsonProperty("endOdometer") val endOdometer: Int,
    @JsonProperty("endPosition") val endPosition: TripPosition,
    @JsonProperty("endTime")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val endTime: ZonedDateTime,
    @JsonProperty("fuelConsumption") val fuelConsumption: Double,
    @JsonProperty("startOdometer") val startOdometer: Int,
    @JsonProperty("startPosition") val startPosition: TripPosition,
    @JsonProperty("startTime")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val startTime: ZonedDateTime,
)

@JvmRecord
data class BoundingBox(
    @JsonProperty("maxLatitude") val maxLatitude: Double,
    @JsonProperty("maxLongitude") val maxLongitude: Double,
    @JsonProperty("minLatitude") val minLatitude: Double,
    @JsonProperty("minLongitude") val minLongitude: Double,
)

@JvmRecord
data class TripPosition(
    @JsonProperty("city") val city: String?,
    @JsonProperty("ISO2CountryCode") val iSO2CountryCode: String,
    @JsonProperty("latitude") val latitude: Double,
    @JsonProperty("longitude") val longitude: Double,
    @JsonProperty("postalCode") val postalCode: String?,
    @JsonProperty("Region") val region: String,
    @JsonProperty("streetAddress") val streetAddress: String?,
)

@JvmRecord
data class TripRoute(
    @JsonProperty("waypoints") val waypoints: List<TripWaypoint>,
)

@JvmRecord
data class TripWaypoint(
    @JsonProperty("timestamp")
    @JsonFormat(shape = STRING, pattern = DATE_FORMAT_PATTERN)
    val timestamp: ZonedDateTime,
    @JsonProperty("odometer") val odometer: Int,
    @JsonProperty("electricalConsumption") val electricalConsumption: Double,
    @JsonProperty("electricalRegeneration") val electricalRegeneration: Double,
    @JsonProperty("fuelConsumption") val fuelConsumption: Double,
    @JsonProperty("position") val position: TripWaypointPosition,
)

@JvmRecord
data class TripWaypointPosition(
    @JsonProperty("latitude") val latitude: Double,
    @JsonProperty("longitude") val longitude: Double,
    @JsonProperty("speed") val speed: Double,
    @JsonProperty("heading") val heading: Double,
)
