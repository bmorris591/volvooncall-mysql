@file:Suppress("TooManyFunctions")

package uk.co.borismorris.volvooncall

import io.micrometer.core.instrument.Metrics.globalRegistry
import io.micrometer.observation.ObservationRegistry
import io.pyroscope.javaagent.api.ConfigurationProvider
import org.jooq.DSLContext
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.WebApplicationType
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.condition.ConditionalOnClass
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty
import org.springframework.boot.autoconfigure.jooq.DefaultConfigurationCustomizer
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.info.GitProperties
import org.springframework.boot.runApplication
import org.springframework.boot.web.client.ClientHttpRequestFactories
import org.springframework.boot.web.client.ClientHttpRequestFactorySettings
import org.springframework.boot.web.client.RestClientCustomizer
import org.springframework.context.ConfigurableApplicationContext
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Profile
import org.springframework.core.env.Environment
import org.springframework.http.client.SimpleClientHttpRequestFactory
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.transaction.annotation.EnableTransactionManagement
import org.springframework.web.client.RestClient
import uk.co.borismorris.volvooncall.config.VocConfig
import uk.co.borismorris.volvooncall.dao.TripRepository
import uk.co.borismorris.volvooncall.dao.TripWaypointRepository
import uk.co.borismorris.volvooncall.dao.VehicleRepository
import uk.co.borismorris.volvooncall.dao.VehicleStatusRepository
import uk.co.borismorris.volvooncall.logging.LogConfigOnStartup
import uk.co.borismorris.volvooncall.logging.LogGitInfoOnStartup
import uk.co.borismorris.volvooncall.pyroscope.Pyroscope
import uk.co.borismorris.volvooncall.pyroscope.SpringConfigProvider
import uk.co.borismorris.volvooncall.service.DaoTripService
import uk.co.borismorris.volvooncall.service.DaoVehicleService
import uk.co.borismorris.volvooncall.service.TripService
import uk.co.borismorris.volvooncall.service.VehicleService
import uk.co.borismorris.volvooncall.vocclient.RestclientVocClient
import uk.co.borismorris.volvooncall.worker.MonitorVolvoOnCall
import java.time.Duration

@SpringBootApplication
@EnableTransactionManagement
class VolvoOnCallApplication {
    @ConfigurationProperties(prefix = "voc")
    @Bean
    fun vocConfig() = VocConfig()

    @Bean
    fun vocClient(config: VocConfig, restclientBuilder: RestClient.Builder) = RestclientVocClient(config, restclientBuilder)

    @Bean
    fun jooqConfigurationCustomizer(@Value("\${spring.jooq.logging:false}") logging: Boolean) = DefaultConfigurationCustomizer {
        it.settings().apply {
            isExecuteLogging = logging
            isDiagnosticsLogging = logging
        }
    }

    @Bean
    fun vehicleRepo(context: DSLContext) = VehicleRepository(context)

    @Bean
    fun vehicleStatsRepo(context: DSLContext) = VehicleStatusRepository(context)

    @Bean
    fun tripRepo(context: DSLContext) = TripRepository(context)

    @Bean
    fun tripWaypointRepo(ccontext: DSLContext) = TripWaypointRepository(ccontext)

    @Bean
    fun restClientCustomizer() = RestClientCustomizer {
        it.requestFactory(
            ClientHttpRequestFactories.get(
                SimpleClientHttpRequestFactory::class.java,
                ClientHttpRequestFactorySettings.DEFAULTS
                    .withConnectTimeout(Duration.ofMinutes(5))
                    .withReadTimeout(Duration.ofMinutes(5)),
            ),
        )
    }

    @Bean
    fun vehicleService(
        vocClient: RestclientVocClient,
        vehicleRepository: VehicleRepository,
        vehicleStatusRepository: VehicleStatusRepository,
    ) = DaoVehicleService(vocClient, vehicleRepository, vehicleStatusRepository)

    @Bean
    fun tripService(
        vocClient: RestclientVocClient,
        tripRepository: TripRepository,
        tripWaypointRepository: TripWaypointRepository,
    ) = DaoTripService(vocClient, tripRepository, tripWaypointRepository)

    @Bean
    @Profile("!test")
    fun monitorVolvoOnCall(
        observationRegistry: ObservationRegistry,
        vocClient: RestclientVocClient,
        vehicleService: VehicleService,
        tripService: TripService,
    ) = MonitorVolvoOnCall(observationRegistry, vocClient, vehicleService, tripService)

    @Bean
    fun logConfigOnStartup(context: ConfigurableApplicationContext) = LogConfigOnStartup(context)

    @Bean
    fun logGitInfoOnStartup(gitProperties: GitProperties?) = LogGitInfoOnStartup(gitProperties)

    @Bean
    @ConditionalOnClass(name = ["io.opentelemetry.javaagent.OpenTelemetryAgent"])
    fun otelRegistry() =
        globalRegistry.registries.find { r -> r.javaClass.name.contains("OpenTelemetryMeterRegistry") }?.also {
            globalRegistry.remove(it)
        }

    @Configuration
    @Profile("!test")
    @EnableScheduling
    class Scheduling

    @Configuration
    @ConditionalOnProperty(prefix = "pyroscope", name = ["enabled"], matchIfMissing = true)
    class PyroscopeConfig {
        @Bean
        fun springConfigProvider(environment: Environment) = SpringConfigProvider(environment)

        @Bean
        fun pyroscope(configurationProvider: ConfigurationProvider) = Pyroscope(configurationProvider)
    }
}

@Suppress("TooGenericExceptionCaught", "SpreadOperator")
fun main(args: Array<String>) {
    runApplication<VolvoOnCallApplication>(*args) {
        webApplicationType = WebApplicationType.NONE
    }
}
