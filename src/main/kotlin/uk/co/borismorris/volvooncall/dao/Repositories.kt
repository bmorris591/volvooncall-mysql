@file:Suppress("UnsafeCallOnNullableType")

package uk.co.borismorris.volvooncall.dao

import io.github.oshai.kotlinlogging.KotlinLogging
import org.geolatte.geom.G2D
import org.geolatte.geom.Point
import org.geolatte.geom.Polygon
import org.geolatte.geom.PositionSequenceBuilders
import org.geolatte.geom.crs.CoordinateReferenceSystems
import org.jooq.DSLContext
import org.jooq.Field
import org.jooq.Fields
import org.jooq.InsertValuesStep9
import org.jooq.Record
import org.jooq.Record7
import org.jooq.RecordMapper
import org.jooq.RowCountQuery
import org.jooq.SelectField
import org.jooq.SelectJoinStep
import org.jooq.impl.DSL.field
import org.jooq.impl.DSL.inline
import org.jooq.impl.DSL.multiset
import org.jooq.types.UInteger
import org.jooq.types.ULong
import org.springframework.transaction.annotation.Transactional
import uk.co.borismorris.volvooncall.generated.database.enums.TripPositionPositionType
import uk.co.borismorris.volvooncall.generated.database.tables.VehicleHeaterTimerStatus
import uk.co.borismorris.volvooncall.generated.database.tables.references.BULB_FAILURE
import uk.co.borismorris.volvooncall.generated.database.tables.references.THEFT_ALARM
import uk.co.borismorris.volvooncall.generated.database.tables.references.TRIP
import uk.co.borismorris.volvooncall.generated.database.tables.references.TRIP_DETAIL
import uk.co.borismorris.volvooncall.generated.database.tables.references.TRIP_METER
import uk.co.borismorris.volvooncall.generated.database.tables.references.TRIP_POSITION
import uk.co.borismorris.volvooncall.generated.database.tables.references.TRIP_WAYPOINT
import uk.co.borismorris.volvooncall.generated.database.tables.references.VEHICLE
import uk.co.borismorris.volvooncall.generated.database.tables.references.VEHICLE_BATTERY_STATUS
import uk.co.borismorris.volvooncall.generated.database.tables.references.VEHICLE_DOOR_STATUS
import uk.co.borismorris.volvooncall.generated.database.tables.references.VEHICLE_HEATER_STATUS
import uk.co.borismorris.volvooncall.generated.database.tables.references.VEHICLE_HEATER_TIMER_STATUS
import uk.co.borismorris.volvooncall.generated.database.tables.references.VEHICLE_STATUS
import uk.co.borismorris.volvooncall.generated.database.tables.references.VEHICLE_TYRE_PRESSURE_STATUS
import uk.co.borismorris.volvooncall.generated.database.tables.references.VEHICLE_WINDOW_STATUS
import uk.co.borismorris.volvooncall.jooq.bind.G2DPoint
import uk.co.borismorris.volvooncall.jooq.bind.G2DPolygon
import uk.co.borismorris.volvooncall.jooq.type.asWkt
import uk.co.borismorris.volvooncall.vocclient.BoundingBox
import uk.co.borismorris.volvooncall.vocclient.Country
import uk.co.borismorris.volvooncall.vocclient.Doors
import uk.co.borismorris.volvooncall.vocclient.Heater
import uk.co.borismorris.volvooncall.vocclient.HeaterAutoTimer
import uk.co.borismorris.volvooncall.vocclient.HvBattery
import uk.co.borismorris.volvooncall.vocclient.RouteDetails
import uk.co.borismorris.volvooncall.vocclient.SeatSelection
import uk.co.borismorris.volvooncall.vocclient.TheftAlarm
import uk.co.borismorris.volvooncall.vocclient.Trip
import uk.co.borismorris.volvooncall.vocclient.TripDetail
import uk.co.borismorris.volvooncall.vocclient.TripPosition
import uk.co.borismorris.volvooncall.vocclient.TripWaypoint
import uk.co.borismorris.volvooncall.vocclient.TripWaypointPosition
import uk.co.borismorris.volvooncall.vocclient.TyrePressure
import uk.co.borismorris.volvooncall.vocclient.Vehicle
import uk.co.borismorris.volvooncall.vocclient.VehicleAttributes
import uk.co.borismorris.volvooncall.vocclient.VehicleStatus
import uk.co.borismorris.volvooncall.vocclient.VehicleUrls
import uk.co.borismorris.volvooncall.vocclient.Windows
import java.net.URI
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneOffset.UTC
import java.time.ZonedDateTime
import kotlin.Double
import org.geolatte.geom.crs.Unit as GeometricUnit
import org.jooq.impl.DSL.count as countStar
import uk.co.borismorris.volvooncall.generated.database.tables.TripPosition as TripPositionTable

private val coordinateReferenceSystem = CoordinateReferenceSystems.mkGeographic(GeometricUnit.DEGREE)
private val invalidUrl = URI.create("https://test.invalid").toURL()
private val logger = KotlinLogging.logger {}

@Transactional(readOnly = true)
class VehicleRepository(
    private val context: DSLContext,
) {
    companion object {
        private val vehicleMapper = RecordMapper<Record, Vehicle> {
            val attributes = VehicleAttributes(
                it[VEHICLE.VEHICLE_ID]!!,
                it[VEHICLE.ASSISTANCE_CALL_SUPPORTED]!!,
                "",
                0,
                it[VEHICLE.CAR_LOCATOR_SUPPORTED]!!,
                0,
                listOf(),
                Country(""),
                "",
                it[VEHICLE.ENGINE_START_SUPPORTED]!!,
                "",
                0,
                "",
                "",
                0,
                0,
                it[VEHICLE.HONK_AND_BLINK_SUPPORTED]!!,
                listOf(),
                "",
                false,
                it[VEHICLE.JOURNAL_LOG_SUPPORTED]!!,
                it[VEHICLE.HIGH_VOLTAGE_BATTERY_SUPPORTED]!!,
                it[VEHICLE.LOCK_SUPPORTED]!!,
                0,
                it[VEHICLE.MODEL_YEAR]!!,
                0,
                it[VEHICLE.OVERRIDE_DELAY_CHARGING_SUPPORTED]!!,
                it[VEHICLE.PRECLIMATIZATION_SUPPORTED]!!,
                it[VEHICLE.REGISTRATION_NUMBER]!!,
                it[VEHICLE.REMOTE_HEATER_SUPPORTED]!!,
                listOf(),
                "",
                it[VEHICLE.STATUS_PARKED_INDOOR_SUPPORTED]!!,
                ZonedDateTime.ofInstant(Instant.EPOCH, UTC),
                ZonedDateTime.ofInstant(Instant.EPOCH, UTC),
                "",
                0,
                0,
                "",
                "",
                "",
                it[VEHICLE.UNLOCK_SUPPORTED]!!,
                0,
                "",
                it[VEHICLE.VEHICLE_TYPE]!!,
                "",
                0,
            )
            val urls = VehicleUrls(attributes.vehicleIdentificationNumber, invalidUrl, invalidUrl, listOf())
            Vehicle(urls, attributes)
        }
    }

    fun count(): Long = context.select(countStar())
        .from(VEHICLE)
        .fetchOne(0, Long::class.java)!!

    fun exists(vehicle: Vehicle): Boolean = context.select(countStar())
        .from(VEHICLE)
        .where(VEHICLE.VEHICLE_ID.eq(vehicle.id))
        .fetchOne(0, Long::class.java)!! > 0

    fun load(vehicleId: String): Vehicle = context.select()
        .from(VEHICLE)
        .where(VEHICLE.VEHICLE_ID.eq(vehicleId))
        .fetchOne()!!
        .map(vehicleMapper)

    fun loadAll(): List<Vehicle> = context.select()
        .from(VEHICLE)
        .fetch()
        .map(vehicleMapper)

    @Transactional(readOnly = false)
    fun save(vehicle: Vehicle): Long = context.insertInto(VEHICLE)
        .values(
            vehicle.id,
            vehicle.attributes.modelYear,
            vehicle.attributes.vehicleType,
            vehicle.attributes.registrationNumber,
            vehicle.attributes.carLocatorSupported,
            vehicle.attributes.honkAndBlinkSupported,
            vehicle.attributes.remoteHeaterSupported,
            vehicle.attributes.unlockSupported,
            vehicle.attributes.lockSupported,
            vehicle.attributes.journalLogSupported,
            vehicle.attributes.assistanceCallSupported,
            vehicle.attributes.highVoltageBatterySupported,
            vehicle.attributes.preclimatizationSupported,
            vehicle.attributes.overrideDelayChargingSupported,
            vehicle.attributes.engineStartSupported,
            vehicle.attributes.statusParkedIndoorSupported,
        )
        .execute().toLong()
}

@Transactional(readOnly = true)
class VehicleStatusRepository(
    private val context: DSLContext,
) {
    companion object {
        const val VEHICLE_STATUS_ID = "vehicle_status_id"

        private val tripMeter1Join = TRIP_METER.`as`("trip_meter_1")
        private val tripMeter2Join = TRIP_METER.`as`("trip_meter_2")
        private val heaterTimer1Join = VEHICLE_HEATER_TIMER_STATUS.`as`("heater_timer_1")
        private val heaterTimer2Join = VEHICLE_HEATER_TIMER_STATUS.`as`("heater_timer_2")

        @Suppress("UNCHECKED_CAST")
        val bulbFailuresJoin = field("bulb_failure") as Field<List<BulbFailure>>

        val bulbFailureMapper = RecordMapper<Record, BulbFailure> {
            BulbFailure(
                it[BULB_FAILURE.BULB_FAILURE_]!!,
                it[BULB_FAILURE.STATUS_TIMESTAMP]!!.atZone(UTC),
            )
        }

        @JvmRecord
        data class BulbFailure(val bulbFailure: String, val timestamp: ZonedDateTime)

        val vehicleStatusMapper = RecordMapper<Record, VehicleStatus> {
            VehicleStatus(
                it[VEHICLE_STATUS.AVERAGE_FUEL_CONSUMPTION]!!,
                it[VEHICLE_STATUS.AVERAGE_FUEL_CONSUMPTION_TIMESTAMP]!!.atZone(UTC),
                it[VEHICLE_STATUS.AVERAGE_SPEED]!!,
                it[VEHICLE_STATUS.AVERAGE_SPEED_TIMESTAMP]!!.atZone(UTC),
                it[VEHICLE_STATUS.BRAKE_FLUID]!!,
                it[VEHICLE_STATUS.BRAKE_FLUID_TIMESTAMP]!!.atZone(UTC),
                it[bulbFailuresJoin].map { b -> b.bulbFailure },
                it[bulbFailuresJoin].firstOrNull()?.timestamp ?: it[VEHICLE_STATUS.VEHICLE_STATUS_TIMESTAMP]!!.atZone(UTC),
                it[VEHICLE_STATUS.CAR_LOCKED]!!,
                it[VEHICLE_STATUS.CAR_LOCKED_TIMESTAMP]!!.atZone(UTC),
                it[VEHICLE_STATUS.CONNECTION_STATUS]!!,
                it[VEHICLE_STATUS.CONNECTION_STATUS_TIMESTAMP]!!.atZone(UTC),
                it[VEHICLE_STATUS.DISTANCE_TO_EMPTY]!!,
                it[VEHICLE_STATUS.DISTANCE_TO_EMPTY_TIMESTAMP]!!.atZone(UTC),
                it.doors(),
                it[VEHICLE_STATUS.ENGINE_RUNNING]!!,
                it[VEHICLE_STATUS.ENGINE_RUNNING_TIMESTAMP]!!.atZone(UTC),
                it[VEHICLE_STATUS.FUEL_AMOUNT]!!,
                it[VEHICLE_STATUS.FUEL_AMOUNT_LEVEL]!!,
                it[VEHICLE_STATUS.FUEL_AMOUNT_LEVEL_TIMESTAMP]!!.atZone(UTC),
                it[VEHICLE_STATUS.FUEL_AMOUNT_TIMESTAMP]!!.atZone(UTC),
                it.heater(),
                it.hvBattery(),
                it[VEHICLE_STATUS.ODOMETER]!!.toInt(),
                it[VEHICLE_STATUS.ODOMETER_TIMESTAMP]!!.atZone(UTC),
                it[VEHICLE_STATUS.PARKED_INDOOR]!!,
                it[VEHICLE_STATUS.PARKED_INDOOR_TIMESTAMP]!!.atZone(UTC),
                it[VEHICLE_STATUS.REMOTE_CLIMATIZATION_STATUS]!!,
                it[VEHICLE_STATUS.REMOTE_CLIMATIZATION_STATUS_TIMESTAMP]!!.atZone(UTC),
                it[VEHICLE_STATUS.SERVICE_WARNING_STATUS]!!,
                it[VEHICLE_STATUS.SERVICE_WARNING_STATUS_TIMESTAMP]!!.atZone(UTC),
                it.theftAlarm(),
                it[VEHICLE_STATUS.TIME_FULLY_ACCESSIBLE_UNTIL]!!.atZone(UTC),
                it[VEHICLE_STATUS.TIME_PARTIALLY_ACCESSIBLE_UNTIL]!!.atZone(UTC),
                it[tripMeter1Join.TRIP_METER_]!!,
                it[tripMeter1Join.STATUS_TIMESTAMP]!!.atZone(UTC),
                it[tripMeter2Join.TRIP_METER_]!!,
                it[tripMeter2Join.STATUS_TIMESTAMP]!!.atZone(UTC),
                it.tyrePressure(),
                it[VEHICLE_STATUS.WASHER_FLUID_LEVEL]!!,
                it[VEHICLE_STATUS.WASHER_FLUID_LEVEL_TIMESTAMP]!!.atZone(UTC),
                it.widows(),
            )
        }

        private fun Record.doors() = Doors(
            get(VEHICLE_DOOR_STATUS.FRONT_LEFT_DOOR_OPEN)!!,
            get(VEHICLE_DOOR_STATUS.FRONT_RIGHT_DOOR_OPEN)!!,
            get(VEHICLE_DOOR_STATUS.HOOD_OPEN)!!,
            get(VEHICLE_DOOR_STATUS.REAR_LEFT_DOOR_OPEN)!!,
            get(VEHICLE_DOOR_STATUS.REAR_RIGHT_DOOR_OPEN)!!,
            get(VEHICLE_DOOR_STATUS.TAILGATE_OPEN)!!,
            get(VEHICLE_DOOR_STATUS.STATUS_TIMESTAMP)!!.atZone(UTC),
        )

        private fun Record.heater() = Heater(
            seatSelection(),
            get(VEHICLE_HEATER_STATUS.HEATER_STATUS)!!,
            heaterTimer1Join.heaterTimerStatus(),
            heaterTimer2Join.heaterTimerStatus(),
            get(VEHICLE_HEATER_STATUS.STATUS_TIMESTAMP)!!.atZone(UTC),
        )

        private fun Record.seatSelection() = SeatSelection(
            get(VEHICLE_HEATER_STATUS.FRONT_DRIVER_SIDE_SEAT_ON)!!,
            get(VEHICLE_HEATER_STATUS.FRONT_PASSENGER_SIDE_SEAT_ON)!!,
            get(VEHICLE_HEATER_STATUS.REAR_DRIVER_SIDE_SEAT_ON)!!,
            get(VEHICLE_HEATER_STATUS.REAR_MID_SEAT_ON)!!,
            get(VEHICLE_HEATER_STATUS.REAR_PASSENGER_SIDE_SEAT_ON)!!,
        )

        context (Record)
        private fun VehicleHeaterTimerStatus.heaterTimerStatus() = HeaterAutoTimer(
            get(STATE)!!,
            get(TIMER_TIME)!!,
        )

        private fun Record.hvBattery() = HvBattery(
            get(VEHICLE_BATTERY_STATUS.DISTANCE_TO_HVBATTERY_EMPTY)!!,
            get(VEHICLE_BATTERY_STATUS.DISTANCE_TO_HVBATTERY_EMPTY_TIMESTAMP)!!.atZone(UTC),
            get(VEHICLE_BATTERY_STATUS.HV_BATTERY_CHARGE_MODE_STATUS)!!,
            get(VEHICLE_BATTERY_STATUS.HV_BATTERY_CHARGE_MODE_STATUS_TIMESTAMP)!!.atZone(UTC),
            get(VEHICLE_BATTERY_STATUS.HV_BATTERY_CHARGE_STATUS)!!,
            get(VEHICLE_BATTERY_STATUS.HV_BATTERY_CHARGE_STATUS_DERIVED)!!,
            get(VEHICLE_BATTERY_STATUS.HV_BATTERY_CHARGE_STATUS_DERIVED_TIMESTAMP)!!.atZone(UTC),
            get(VEHICLE_BATTERY_STATUS.HV_BATTERY_CHARGE_STATUS_TIMESTAMP)!!.atZone(UTC),
            get(VEHICLE_BATTERY_STATUS.HV_BATTERY_CHARGE_WARNING)!!,
            get(VEHICLE_BATTERY_STATUS.HV_BATTERY_CHARGE_WARNING_TIMESTAMP)!!.atZone(UTC),
            get(VEHICLE_BATTERY_STATUS.HV_BATTERY_LEVEL)!!,
            get(VEHICLE_BATTERY_STATUS.HV_BATTERY_LEVEL_TIMESTAMP)!!.atZone(UTC),
            get(VEHICLE_BATTERY_STATUS.TIME_TO_HVBATTERY_FULLY_CHARGED)!!,
            get(VEHICLE_BATTERY_STATUS.TIME_TO_HVBATTERY_FULLY_CHARGED_TIMESTAMP)!!.atZone(UTC),
        )

        private fun Record.theftAlarm() = get(THEFT_ALARM.VEHICLE_STATUS_ID)?.let {
            val gpsPosition = get(THEFT_ALARM.GPS_POSITION, G2DPoint::class.java).point
            TheftAlarm(
                gpsPosition.position.lat,
                gpsPosition.position.lon,
                get(THEFT_ALARM.ALARM_TRIGGER_TIMESTAMP)!!.atZone(UTC),
            )
        }

        private fun Record.tyrePressure() = TyrePressure(
            get(VEHICLE_TYRE_PRESSURE_STATUS.FRONT_LEFT_PRESSURE_STATUS)!!,
            get(VEHICLE_TYRE_PRESSURE_STATUS.FRONT_RIGHT_PRESSURE_STATUS)!!,
            get(VEHICLE_TYRE_PRESSURE_STATUS.REAR_LEFT_PRESSURE_STATUS)!!,
            get(VEHICLE_TYRE_PRESSURE_STATUS.REAR_RIGHT_PRESSURE_STATUS)!!,
            get(VEHICLE_TYRE_PRESSURE_STATUS.STATUS_TIMESTAMP)!!.atZone(UTC),
        )

        private fun Record.widows() = Windows(
            get(VEHICLE_WINDOW_STATUS.FRONT_LEFT_WINDOW_OPEN)!!,
            get(VEHICLE_WINDOW_STATUS.FRONT_RIGHT_WINDOW_OPEN)!!,
            get(VEHICLE_WINDOW_STATUS.REAR_LEFT_WINDOW_OPEN)!!,
            get(VEHICLE_WINDOW_STATUS.REAR_RIGHT_WINDOW_OPEN)!!,
            get(VEHICLE_WINDOW_STATUS.STATUS_TIMESTAMP)!!.atZone(UTC),
        )
    }

    @Transactional(readOnly = false)
    fun save(vehicle: Vehicle, vehicleStatus: VehicleStatus, timestamp: Instant): VehicleStatusSaved = with(vehicleStatus) {
        val saved = saveStatus(vehicle, timestamp)
        check(saved.updated == 1L) { "Expected to update 1 row with insert, updated $saved." }
        val lastInsertId = saved.id
        logger.info { "Saved vehicle status data, auto generated id $lastInsertId." }
        val updated = saveStatusDetails(lastInsertId)
            .sumOf { it.toLong() }
        VehicleStatusSaved(lastInsertId, saved.updated + updated)
    }

    fun count(vehicle: Vehicle): Long = context.select(countStar())
        .from(VEHICLE_STATUS)
        .where(VEHICLE_STATUS.VEHICLE_ID.eq(vehicle.urls.vehicleId))
        .fetchOne(0, Long::class.java)!!

    fun load(vehicle: Vehicle): List<VehicleStatus> = selectVehicleStatus()
        .where(VEHICLE_STATUS.VEHICLE_ID.eq(vehicle.id))
        .orderBy(VEHICLE_STATUS.VEHICLE_STATUS_TIMESTAMP.desc())
        .fetch()
        .map(vehicleStatusMapper)

    private fun selectVehicleStatus(): SelectJoinStep<out Record> {
        fun Fields.asSequence() = fields().asSequence()

        val columns = sequence<SelectField<*>> {
            yieldAll(VEHICLE_STATUS.asSequence())
            yieldAll(THEFT_ALARM.asSequence())
            yieldAll(VEHICLE_BATTERY_STATUS.asSequence())
            yieldAll(VEHICLE_DOOR_STATUS.asSequence())
            yieldAll(VEHICLE_TYRE_PRESSURE_STATUS.asSequence())
            yieldAll(VEHICLE_WINDOW_STATUS.asSequence())
            yieldAll(VEHICLE_HEATER_STATUS.asSequence())
            yieldAll(tripMeter1Join.asSequence())
            yieldAll(tripMeter2Join.asSequence())
            yieldAll(heaterTimer1Join.asSequence())
            yieldAll(heaterTimer2Join.asSequence())
            yield(
                multiset(
                    context.select()
                        .from(BULB_FAILURE)
                        .where(VEHICLE_STATUS.VEHICLE_STATUS_ID.eq(BULB_FAILURE.VEHICLE_STATUS_ID)),
                ).`as`(bulbFailuresJoin).convertFrom { it.map(bulbFailureMapper) },
            )
        }

        return context.select(columns.toList()).from(VEHICLE_STATUS)
            .leftJoin(THEFT_ALARM).on(VEHICLE_STATUS.VEHICLE_STATUS_ID.eq(THEFT_ALARM.VEHICLE_STATUS_ID))
            .leftJoin(VEHICLE_BATTERY_STATUS).on(VEHICLE_STATUS.VEHICLE_STATUS_ID.eq(VEHICLE_BATTERY_STATUS.VEHICLE_STATUS_ID))
            .leftJoin(VEHICLE_DOOR_STATUS).on(VEHICLE_STATUS.VEHICLE_STATUS_ID.eq(VEHICLE_DOOR_STATUS.VEHICLE_STATUS_ID))
            .leftJoin(VEHICLE_TYRE_PRESSURE_STATUS).on(VEHICLE_STATUS.VEHICLE_STATUS_ID.eq(VEHICLE_TYRE_PRESSURE_STATUS.VEHICLE_STATUS_ID))
            .leftJoin(VEHICLE_WINDOW_STATUS).on(VEHICLE_STATUS.VEHICLE_STATUS_ID.eq(VEHICLE_WINDOW_STATUS.VEHICLE_STATUS_ID))
            .leftJoin(VEHICLE_HEATER_STATUS).on(VEHICLE_STATUS.VEHICLE_STATUS_ID.eq(VEHICLE_HEATER_STATUS.VEHICLE_STATUS_ID))
            .leftJoin(tripMeter1Join).on(VEHICLE_STATUS.VEHICLE_STATUS_ID.eq(tripMeter1Join.VEHICLE_STATUS_ID).and(tripMeter1Join.TRIP_METER_ID.eq(1.uByte())))
            .leftJoin(tripMeter2Join).on(VEHICLE_STATUS.VEHICLE_STATUS_ID.eq(tripMeter2Join.VEHICLE_STATUS_ID).and(tripMeter2Join.TRIP_METER_ID.eq(2.uByte())))
            .leftJoin(heaterTimer1Join).on(VEHICLE_STATUS.VEHICLE_STATUS_ID.eq(heaterTimer1Join.VEHICLE_STATUS_ID).and(heaterTimer1Join.TIMER_ID.eq(1.uByte())))
            .leftJoin(heaterTimer2Join).on(VEHICLE_STATUS.VEHICLE_STATUS_ID.eq(heaterTimer2Join.VEHICLE_STATUS_ID).and(heaterTimer2Join.TIMER_ID.eq(2.uByte())))
    }

    context (VehicleStatus)
    private fun saveStatus(vehicle: Vehicle, timestamp: Instant): VehicleStatusSaved = context.insertInto(VEHICLE_STATUS).values(
        null,
        LocalDateTime.ofInstant(timestamp, UTC),
        vehicle.id,
        averageFuelConsumption,
        averageFuelConsumptionTimestamp.toUtc(),
        averageSpeed,
        averageSpeedTimestamp.toUtc(),
        brakeFluid,
        brakeFluidTimestamp.toUtc(),
        carLocked,
        carLockedTimestamp.toUtc(),
        connectionStatus,
        connectionStatusTimestamp.toUtc(),
        distanceToEmpty,
        distanceToEmptyTimestamp.toUtc(),
        engineRunning,
        engineRunningTimestamp.toUtc(),
        fuelAmount,
        fuelAmountTimestamp.toUtc(),
        fuelAmountLevel,
        fuelAmountLevelTimestamp.toUtc(),
        odometer,
        odometerTimestamp.toUtc(),
        parkedIndoor,
        parkedIndoorTimestamp.toUtc(),
        remoteClimatizationStatus,
        remoteClimatizationStatusTimestamp.toUtc(),
        serviceWarningStatus,
        serviceWarningStatusTimestamp.toUtc(),
        timeFullyAccessibleUntil.toUtc(),
        timeFullyAccessibleUntil.toUtc(),
        washerFluidLevel,
        washerFluidLevelTimestamp.toUtc(),
    )
        .returning(VEHICLE_STATUS.VEHICLE_STATUS_ID)
        .fetchOne()!!
        .map { VehicleStatusSaved(it[VEHICLE_STATUS.VEHICLE_STATUS_ID]!!.toInt(), 1) }

    context (VehicleStatus)
    private fun saveStatusDetails(statusId: Int): IntArray = context.batch(
        sequence {
            yield(saveTripMeters(statusId))
            saveTheftAlarm(statusId)?.also { yield(it) }
            saveBulbFailures(statusId)?.also { yield(it) }
            with(hvBattery) { yield(saveBatteryStatus(statusId)) }
            with(doors) { yield(saveDoorStatus(statusId)) }
            with(heater) { yieldAll(saveHeaterStatus(statusId)) }
            with(tyrePressure) { yield(saveTyrePressureStatus(statusId)) }
            with(windows) { yield(saveWindowStatus(statusId)) }
        }.toList(),
    ).execute()

    context (HvBattery)
    private fun saveBatteryStatus(statusId: Int): RowCountQuery = context.insertInto(VEHICLE_BATTERY_STATUS).values(
        statusId.uInt(),
        distanceToHVBatteryEmpty,
        distanceToHVBatteryEmptyTimestamp.toUtc(),
        hvBatteryChargeModeStatus,
        hvBatteryChargeModeStatusTimestamp.toUtc(),
        hvBatteryChargeStatus,
        hvBatteryChargeStatusTimestamp.toUtc(),
        hvBatteryChargeStatusDerived,
        hvBatteryChargeStatusDerivedTimestamp?.toUtc(),
        hvBatteryChargeWarning,
        hvBatteryChargeWarningTimestamp.toUtc(),
        hvBatteryLevel,
        hvBatteryLevelTimestamp.toUtc(),
        timeToHVBatteryFullyCharged,
        timeToHVBatteryFullyChargedTimestamp.toUtc(),
    )

    context (VehicleStatus)
    private fun saveTripMeters(statusId: Int): RowCountQuery = context.insertInto(TRIP_METER)
        .values(statusId.uInt(), 1, tripMeter1, tripMeter1Timestamp.toUtc())
        .values(statusId.uInt(), 2, tripMeter2, tripMeter2Timestamp.toUtc())

    context (VehicleStatus)
    private fun saveTheftAlarm(statusId: Int): RowCountQuery? = theftAlarm?.let {
        context.insertInto(THEFT_ALARM).values(
            statusId.uInt(),
            G2DPoint(Point(G2D(it.longitude, it.latitude), coordinateReferenceSystem)),
            it.timestamp.toUtc(),
        )
    }

    context (VehicleStatus)
    private fun saveBulbFailures(statusId: Int): RowCountQuery? {
        if (bulbFailures.isEmpty()) {
            return null
        }

        val insert = context.insertInto(
            BULB_FAILURE,
            BULB_FAILURE.VEHICLE_STATUS_ID,
            BULB_FAILURE.BULB_FAILURE_,
            BULB_FAILURE.STATUS_TIMESTAMP,
        )

        return bulbFailures.fold(insert) { i, failure ->
            i.values(
                statusId.uInt(),
                failure,
                bulbFailuresTimestamp.toUtc(),
            )
        }
    }

    context (Doors)
    private fun saveDoorStatus(statusId: Int): RowCountQuery = context.insertInto(VEHICLE_DOOR_STATUS).values(
        statusId,
        frontLeftDoorOpen,
        frontRightDoorOpen,
        rearLeftDoorOpen,
        rearRightDoorOpen,
        hoodOpen,
        tailgateOpen,
        timestamp.toUtc(),
    )

    context (Heater)
    private fun saveHeaterStatus(statusId: Int): Sequence<RowCountQuery> {
        val insertStatus = context.insertInto(VEHICLE_HEATER_STATUS).values(
            statusId,
            status,
            seatSelection.frontDriverSide,
            seatSelection.frontPassengerSide,
            seatSelection.rearDriverSide,
            seatSelection.rearPassengerSide,
            seatSelection.rearMid,
            timestamp.toUtc(),
        )
        return sequenceOf(insertStatus, saveHeaterTimers(statusId, 1 to timer1, 2 to timer2))
    }

    private fun saveHeaterTimers(statusId: Int, vararg timerStatus: Pair<Int, HeaterAutoTimer>): RowCountQuery {
        val insert = context.insertInto(
            VEHICLE_HEATER_TIMER_STATUS,
            VEHICLE_HEATER_TIMER_STATUS.VEHICLE_STATUS_ID,
            VEHICLE_HEATER_TIMER_STATUS.TIMER_ID,
            VEHICLE_HEATER_TIMER_STATUS.STATE,
            VEHICLE_HEATER_TIMER_STATUS.TIMER_TIME,
        )

        return timerStatus.fold(insert) { i, (timerId, timer) ->
            i.values(
                statusId.uInt(),
                timerId.uByte(),
                timer.state,
                timer.time,
            )
        }
    }

    context (TyrePressure)
    private fun saveTyrePressureStatus(statusId: Int): RowCountQuery = context.insertInto(VEHICLE_TYRE_PRESSURE_STATUS).values(
        statusId,
        frontLeftTyrePressure,
        frontRightTyrePressure,
        rearLeftTyrePressure,
        rearRightTyrePressure,
        timestamp.toUtc(),
    )

    context (Windows)
    private fun saveWindowStatus(statusId: Int): RowCountQuery = context.insertInto(VEHICLE_WINDOW_STATUS).values(
        statusId,
        frontLeftWindowOpen,
        frontRightWindowOpen,
        rearLeftWindowOpen,
        rearRightWindowOpen,
        timestamp.toUtc(),
    )

    @JvmRecord
    data class VehicleStatusSaved(val id: Int, val updated: Long)
}

typealias TripRecord = Record7<ULong?, String?, String?, UInteger?, String?, G2DPolygon?, List<TripDetail>>
typealias InsertTripPosition = InsertValuesStep9<Record, ULong?, UInteger?, TripPositionPositionType?, String?, String?, String?, String?, String?, G2DPoint?>
typealias InsertWaypoint = InsertValuesStep9<Record, ULong?, LocalDateTime?, Double?, Double?, Double?, Double?, Double?, ULong?, G2DPoint?>

@Transactional(readOnly = true)
class TripRepository(
    private val context: DSLContext,
) {

    companion object {
        private val startPosition = TRIP_POSITION.`as`("start_position")
        private val endPosition = TRIP_POSITION.`as`("end_position")

        @Suppress("UNCHECKED_CAST")
        private val tripDetails = field("trip_detail") as Field<List<TripDetail>>

        private val tripDetailMapper = RecordMapper<Record, TripDetail> { row ->
            fun Record.loadTripPosition(table: TripPositionTable): TripPosition {
                @Suppress("UNCHECKED_CAST")
                val gpsPosition = get(table.GPS_POSITION.asWkt()).point
                return TripPosition(
                    get(table.CITY),
                    get(table.COUNTRY)!!,
                    gpsPosition.position.lat,
                    gpsPosition.position.lon,
                    get(table.POST_CODE),
                    get(table.REGION)!!,
                    get(table.STREET_ADDRESS),
                )
            }
            TripDetail(
                row.get(TRIP_DETAIL.DISTANCE)!!,
                row.get(TRIP_DETAIL.ELECTRICAL_CONSUMPTION)!!,
                row.get(TRIP_DETAIL.ELECTRICAL_REGENERATION)!!,
                row.get(TRIP_DETAIL.END_ODOMETER)!!.toInt(),
                row.loadTripPosition(endPosition),
                row.get(TRIP_DETAIL.END_TIME)!!.atZone(UTC),
                row.get(TRIP_DETAIL.FUEL_CONSUMPTION)!!,
                row.get(TRIP_DETAIL.START_ODOMETER)!!.toInt(),
                row.loadTripPosition(startPosition),
                row.get(TRIP_DETAIL.START_TIME)!!.atZone(UTC),
            )
        }

        val tripMapper = RecordMapper<TripRecord, Trip> { row ->
            fun Record.toRouteDetails() = get(TRIP.BOUNDING_BOX)?.let { polygon ->
                val box = polygon.polygon
                box.boundingBox.let {
                    val maxCorner = it.upperRight()
                    val minCorner = it.lowerLeft()
                    BoundingBox(
                        maxCorner.lat,
                        maxCorner.lon,
                        minCorner.lat,
                        minCorner.lon,
                    )
                }
            }?.let {
                RouteDetails(
                    it,
                    invalidUrl,
                    get(TRIP.WAYPOINTS)!!.toInt(),
                )
            }

            Trip(
                row.get(TRIP.CATEGORY)!!,
                row.get(TRIP.TRIP_ID)!!.toLong(),
                row.get(TRIP.NAME),
                row.toRouteDetails(),
                invalidUrl,
                row.get(tripDetails),
                row.get(TRIP.NOTES),
            )
        }
    }

    private val joinPosFields = sequence {
        yieldAll(TRIP_DETAIL.fields().asSequence())
        yieldAll(startPosition.posFields())
        yieldAll(endPosition.posFields())
    }.toList()

    private fun TripPositionTable.posFields() = sequenceOf(
        TRIP_ID,
        TRIP_LEG,
        POSITION_TYPE,
        CITY,
        COUNTRY,
        STREET_ADDRESS,
        REGION,
        POST_CODE,
        GPS_POSITION.asWkt(),
    )

    fun count(): Long = context.select(countStar())
        .from(TRIP)
        .fetchOne(0, Long::class.java)!!

    fun exists(trip: Trip): Boolean = context.select(countStar())
        .from(TRIP)
        .where(TRIP.TRIP_ID.eq(trip.id.uLong()))
        .fetchOne(0, Long::class.java)!! > 0

    fun load(id: Long): Trip = selectTrips()
        .where(TRIP.TRIP_ID.eq(id.uLong()))
        .fetchOne()!!
        .toTrip()

    fun loadAllStartedAfter(vehicle: Vehicle, start: ZonedDateTime): List<Trip> = selectTrips()
        .where(
            TRIP.TRIP_ID.`in`(
                context.selectDistinct(TRIP_DETAIL.TRIP_ID)
                    .from(TRIP_DETAIL)
                    .where(TRIP_DETAIL.START_TIME.greaterOrEqual(start.toUtc())),
            ).and(TRIP.VEHICLE_ID.eq(vehicle.id)),
        )
        .forUpdate()
        .fetch()
        .map { it.toTrip() }

    fun loadAll(): List<Trip> = selectTrips()
        .fetch()
        .map { it.toTrip() }

    fun loadAll(vehicle: Vehicle): List<Trip> = selectTrips()
        .where(TRIP.VEHICLE_ID.eq(vehicle.id))
        .fetch()
        .map { it.toTrip() }

    private fun TripRecord.toTrip(): Trip = tripMapper.map(this)!!

    private fun selectTrips(): SelectJoinStep<out TripRecord> {
        fun <R : Record> SelectJoinStep<out R>.joinPos(position: TripPositionTable, type: TripPositionPositionType) = join(position)
            .on(
                position.TRIP_ID.eq(TRIP_DETAIL.TRIP_ID)
                    .and(position.TRIP_LEG.eq(TRIP_DETAIL.TRIP_LEG))
                    .and(position.POSITION_TYPE.eq(inline(type))),
            )

        return context.select(
            TRIP.TRIP_ID,
            TRIP.CATEGORY,
            TRIP.NAME,
            TRIP.WAYPOINTS,
            TRIP.NOTES,
            TRIP.BOUNDING_BOX,
            multiset(
                context.select(joinPosFields)
                    .from(TRIP_DETAIL)
                    .joinPos(startPosition, TripPositionPositionType.START)
                    .joinPos(endPosition, TripPositionPositionType.END)
                    .where(TRIP_DETAIL.TRIP_ID.eq(TRIP.TRIP_ID)),
            ).`as`(tripDetails).convertFrom { it.map(tripDetailMapper) },
        ).from(TRIP)
    }

    @Suppress("CyclomaticComplexMethod")
    @Transactional(readOnly = false)
    fun save(vehicle: Vehicle, trip: Trip): Long {
        fun RouteDetails.polygon() = with(boundingBox) {
            PositionSequenceBuilders.fixedSized(5, G2D::class.java)
                .add(G2D(maxLongitude, minLatitude))
                .add(G2D(maxLongitude, maxLatitude))
                .add(G2D(minLongitude, maxLatitude))
                .add(G2D(minLongitude, minLatitude))
                .add(G2D(maxLongitude, minLatitude))
                .toPositionSequence()
        }.let { Polygon(it, coordinateReferenceSystem) }

        fun insertTrip(trip: Trip, vehicle: Vehicle): RowCountQuery = context.insertInto(TRIP).values(
            vehicle.id,
            trip.id,
            trip.category,
            trip.name,
            trip.userNotes,
            trip.routeDetails?.totalWaypoints ?: 0,
            trip.routeDetails?.polygon()?.let { G2DPolygon(it) },
        )

        fun insertTripDetails(details: Iterable<IndexedValue<TripDetail>>): RowCountQuery {
            val insert = context.insertInto(
                TRIP_DETAIL,
                TRIP_DETAIL.TRIP_ID,
                TRIP_DETAIL.TRIP_LEG,
                TRIP_DETAIL.DISTANCE,
                TRIP_DETAIL.ELECTRICAL_CONSUMPTION,
                TRIP_DETAIL.FUEL_CONSUMPTION,
                TRIP_DETAIL.ELECTRICAL_REGENERATION,
                TRIP_DETAIL.START_ODOMETER,
                TRIP_DETAIL.END_ODOMETER,
                TRIP_DETAIL.START_TIME,
                TRIP_DETAIL.END_TIME,
            )

            return details.fold(insert) { i, (tripLeg, tripDetails) ->
                i.values(
                    trip.id.uLong(),
                    tripLeg.uInt(),
                    tripDetails.distance,
                    tripDetails.electricalConsumption,
                    tripDetails.fuelConsumption,
                    tripDetails.electricalRegeneration,
                    tripDetails.startOdometer.uLong(),
                    tripDetails.endOdometer.uLong(),
                    tripDetails.startTime.toUtc(),
                    tripDetails.endTime.toUtc(),
                )
            }
        }

        fun insertTripPositions(details: Iterable<IndexedValue<TripDetail>>): RowCountQuery {
            fun InsertTripPosition.tripPositionValues(tripLeg: Int, position: TripPosition, type: TripPositionPositionType) = values(
                trip.id.uLong(),
                tripLeg.uInt(),
                type,
                position.city,
                position.iSO2CountryCode,
                position.streetAddress,
                position.region,
                position.postalCode,
                G2DPoint(Point(G2D(position.longitude, position.latitude), coordinateReferenceSystem)),
            )

            val insert = context.insertInto(
                TRIP_POSITION,
                TRIP_POSITION.TRIP_ID,
                TRIP_POSITION.TRIP_LEG,
                TRIP_POSITION.POSITION_TYPE,
                TRIP_POSITION.CITY,
                TRIP_POSITION.COUNTRY,
                TRIP_POSITION.STREET_ADDRESS,
                TRIP_POSITION.REGION,
                TRIP_POSITION.POST_CODE,
                TRIP_POSITION.GPS_POSITION,
            )

            return details.fold(insert) { i, (tripLeg, tripDetails) ->
                i
                    .tripPositionValues(tripLeg, tripDetails.startPosition, TripPositionPositionType.START)
                    .tripPositionValues(tripLeg, tripDetails.endPosition, TripPositionPositionType.END)
            }
        }

        val details = trip.tripDetails.withIndex()
        return context.batch(
            sequence {
                yield(insertTrip(trip, vehicle))
                yield(insertTripDetails(details))
                yield(insertTripPositions(details))
            }.toList(),
        ).execute().sumOf { it.toLong() }
    }

    fun deleteById(id: Long): Long = context.deleteFrom(TRIP)
        .where(TRIP.TRIP_ID.eq(id.uLong()))
        .execute()
        .toLong()
}

fun TripRepository.saveAll(vehicle: Vehicle, trips: Iterable<Trip>) {
    trips.forEach { save(vehicle, it) }
}

@Transactional(readOnly = true)
class TripWaypointRepository(
    private val context: DSLContext,
) {
    companion object {
        private val tripWaypointMapper = RecordMapper<Record, TripWaypoint> { row ->
            val gpsPosition = row.get(TRIP_WAYPOINT.GPS_POSITION, G2DPoint::class.java).point
            TripWaypoint(
                row.get(TRIP_WAYPOINT.WAYPOINT_TIMESTAMP)!!.atZone(UTC),
                row.get(TRIP_WAYPOINT.ODOMETER)!!.toInt(),
                row.get(TRIP_WAYPOINT.ELECTRICAL_CONSUMPTION, Double::class.java)!!,
                row.get(TRIP_WAYPOINT.ELECTRICAL_REGENERATION, Double::class.java)!!,
                row.get(TRIP_WAYPOINT.FUEL_CONSUMPTION, Double::class.java)!!,
                TripWaypointPosition(
                    gpsPosition.position.lat,
                    gpsPosition.position.lon,
                    row.get(TRIP_WAYPOINT.SPEED, Double::class.java)!!,
                    row.get(TRIP_WAYPOINT.HEADING, Double::class.java)!!,
                ),
            )
        }
    }

    @Transactional(readOnly = false)
    fun save(trip: Trip, waypoints: Sequence<TripWaypoint>): Long = waypoints
        .chunked(1_000)
        .map { saveBatch(trip, it) }
        .sumOf { it.toLong() }

    private fun saveBatch(trip: Trip, waypoints: List<TripWaypoint>): Int {
        fun insertWaypoint(): InsertWaypoint = context.insertInto(
            TRIP_WAYPOINT,
            TRIP_WAYPOINT.TRIP_ID,
            TRIP_WAYPOINT.WAYPOINT_TIMESTAMP,
            TRIP_WAYPOINT.ELECTRICAL_CONSUMPTION,
            TRIP_WAYPOINT.FUEL_CONSUMPTION,
            TRIP_WAYPOINT.ELECTRICAL_REGENERATION,
            TRIP_WAYPOINT.SPEED,
            TRIP_WAYPOINT.HEADING,
            TRIP_WAYPOINT.ODOMETER,
            TRIP_WAYPOINT.GPS_POSITION,
        )

        if (waypoints.isEmpty()) {
            return 0
        }

        return waypoints
            .fold(insertWaypoint()) { i, w ->
                i.values(
                    trip.id.uLong(),
                    w.timestamp.toUtc(),
                    w.electricalConsumption,
                    w.fuelConsumption,
                    w.electricalRegeneration,
                    w.position.speed,
                    w.position.heading,
                    w.odometer.uLong(),
                    G2DPoint(Point(G2D(w.position.longitude, w.position.latitude), coordinateReferenceSystem)),
                )
            }
            .execute()
    }

    fun count(trip: Trip): Long = context.select(countStar())
        .from(TRIP_WAYPOINT)
        .where(TRIP_WAYPOINT.TRIP_ID.equal(trip.id.uLong()))
        .fetchOne(0, Long::class.java)!!

    fun load(tripId: Long): List<TripWaypoint> = context.select()
        .from(TRIP_WAYPOINT)
        .where(TRIP_WAYPOINT.TRIP_ID.eq(tripId.uLong()))
        .fetch()
        .map { row -> row.map(tripWaypointMapper) }
}

private fun ZonedDateTime.toUtc() = withZoneSameInstant(UTC).toLocalDateTime()
