package uk.co.borismorris.volvooncall.dao

import org.jooq.types.UByte
import org.jooq.types.UInteger
import org.jooq.types.ULong

fun Long.uLong(): ULong = ULong.valueOf(this)
fun Int.uLong(): ULong = ULong.valueOf(this.toLong())
fun Int.uInt(): UInteger = UInteger.valueOf(this)
fun Int.uByte(): UByte = UByte.valueOf(this)
