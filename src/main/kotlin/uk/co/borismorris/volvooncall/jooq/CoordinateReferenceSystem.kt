package uk.co.borismorris.volvooncall.jooq

import org.geolatte.geom.G2D
import org.geolatte.geom.crs.CoordinateReferenceSystems
import org.geolatte.geom.crs.GeographicCoordinateReferenceSystem
import org.geolatte.geom.crs.Unit

val coordinateReferenceSystem: GeographicCoordinateReferenceSystem<G2D> = CoordinateReferenceSystems.mkGeographic(Unit.DEGREE)
