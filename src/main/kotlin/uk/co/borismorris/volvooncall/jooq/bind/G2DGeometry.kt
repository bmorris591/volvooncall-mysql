package uk.co.borismorris.volvooncall.jooq.bind

import org.geolatte.geom.G2D
import org.geolatte.geom.Geometry
import org.geolatte.geom.Point
import org.geolatte.geom.Polygon

sealed interface G2DGeometry<out G : Geometry<G2D>> {
    val geometry: G
}

@JvmInline
value class G2DPoint(val point: Point<G2D>) : G2DGeometry<Point<G2D>> {
    override val geometry: Point<G2D>
        get() = point
}

@JvmInline
value class G2DPolygon(val polygon: Polygon<G2D>) : G2DGeometry<Polygon<G2D>> {
    override val geometry: Polygon<G2D>
        get() = polygon
}

class PointBinding : G2DGeometryBinding<G2DPoint>(G2DPoint::class.java, { G2DPoint(it as Point<G2D>) })

class PolygonBinding : G2DGeometryBinding<G2DPolygon>(G2DPolygon::class.java, { G2DPolygon(it as Polygon<G2D>) })
