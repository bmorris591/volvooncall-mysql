CREATE SCHEMA IF NOT EXISTS `volvooncall` DEFAULT CHARACTER SET utf8mb4;
GRANT ALL PRIVILEGES ON volvooncall.* TO 'volvooncall'@'%' IDENTIFIED BY 'password';
GRANT SELECT ON volvooncall.* TO 'grafana'@'%' IDENTIFIED BY 'password';
