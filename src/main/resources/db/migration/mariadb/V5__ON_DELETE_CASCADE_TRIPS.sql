ALTER TABLE `trip_detail`
    DROP CONSTRAINT `trip_detail_ibfk_1`;
ALTER TABLE `trip_detail`
    ADD CONSTRAINT `trip_detail_trip_trip_id` FOREIGN KEY (`trip_id`) REFERENCES `trip` (`trip_id`) ON DELETE CASCADE;

ALTER TABLE `trip_position`
    DROP CONSTRAINT `trip_position_ibfk_1`;
ALTER TABLE `trip_position`
    ADD CONSTRAINT `trip_position_trip_detail_trip_id_trip_leg` FOREIGN KEY (`trip_id`, `trip_leg`) REFERENCES `trip_detail` (`trip_id`, `trip_leg`) ON DELETE CASCADE;

ALTER TABLE `trip_waypoint`
    DROP CONSTRAINT `trip_waypoint_ibfk_1`;
ALTER TABLE `trip_waypoint`
    ADD CONSTRAINT `trip_waypoint_trip_trip_id` FOREIGN KEY (`trip_id`) REFERENCES `trip` (`trip_id`) ON DELETE CASCADE;
