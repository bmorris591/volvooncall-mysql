ALTER TABLE `vehicle_status`
    DROP COLUMN `theft_alarm`;

CREATE TABLE `theft_alarm`
(
    `vehicle_status_id`       INT UNSIGNED NOT NULL,
    `gps_position`            POINT        NOT NULL,
    `alarm_trigger_timestamp` DATETIME     NOT NULL DEFAULT 0,
    PRIMARY KEY (`vehicle_status_id`),
    FOREIGN KEY (`vehicle_status_id`) REFERENCES `vehicle_status` (`vehicle_status_id`),
    INDEX (`alarm_trigger_timestamp`)
) ENGINE = InnoDB
  DEFAULT CHARSET = utf8mb4
  COLLATE = utf8mb4_unicode_ci;
