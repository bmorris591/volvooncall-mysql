import de.undercouch.gradle.tasks.download.Download
import io.gitlab.arturbosch.detekt.CONFIGURATION_DETEKT
import io.gitlab.arturbosch.detekt.Detekt
import io.gitlab.arturbosch.detekt.getSupportedKotlinVersion
import org.flywaydb.core.Flyway
import org.gradle.api.tasks.testing.logging.TestExceptionFormat.FULL
import org.jetbrains.kotlin.gradle.dsl.JvmTarget
import org.jetbrains.kotlin.gradle.tasks.KotlinJvmCompile
import org.jooq.codegen.GenerationTool
import org.jooq.meta.jaxb.Configuration
import org.jooq.meta.jaxb.Database
import org.jooq.meta.jaxb.ForcedType
import org.jooq.meta.jaxb.Generate
import org.jooq.meta.jaxb.Generator
import org.jooq.meta.jaxb.Jdbc
import org.jooq.meta.jaxb.Target
import org.testcontainers.containers.JdbcDatabaseContainer
import org.testcontainers.containers.MariaDBContainerProvider

buildscript {
    dependencies {
        classpath(libs.testcontainers.mariadb)
        classpath(libs.mariadb.jdbc)
        classpath(libs.flyway.mysql)
        classpath(libs.jooq.codegen)
    }
}

plugins {
    java
    application
    idea
    `project-report`
    alias(libs.plugins.spring.boot)
    alias(libs.plugins.graalvm)
    alias(libs.plugins.kotlin.jvm)
    alias(libs.plugins.kotlin.spring)
    alias(libs.plugins.git.props)
    alias(libs.plugins.jib)
    alias(libs.plugins.versions)
    alias(libs.plugins.catalog.update)

    alias(libs.plugins.download)

    alias(libs.plugins.spotless)
    alias(libs.plugins.detekt)
}

apply(plugin = "io.spring.dependency-management")

group = "uk.co.borismorris.volvooncall"
val mainClassKt = "uk.co.borismorris.volvooncall.VolvoOnCallApplicationKt"

java {
    sourceCompatibility = JavaVersion.VERSION_21
    targetCompatibility = JavaVersion.VERSION_21
}

val jooqDir = layout.buildDirectory.dir("generated/jooq")
val jooqCodegen by tasks.registering(JooqCodegenTask::class) {
    migrations = file("src/main/resources/db/migration/mariadb")
    outputDirectory = jooqDir
    packageName = "uk.co.borismorris.volvooncall.generated.database"
}

kotlin {
    sourceSets {
        main {
            kotlin.srcDir(jooqCodegen)
        }
    }
}

tasks.withType<KotlinJvmCompile> {
    compilerOptions {
        jvmTarget = JvmTarget.JVM_21
        allWarningsAsErrors = true
        freeCompilerArgs.addAll(
            "-Xjsr305=strict",
            "-Xcontext-receivers",
        )
    }
}

kotlin {
    sourceSets.all {
        languageSettings {
            progressiveMode = true
            optIn("kotlin.RequiresOptIn")
            optIn("kotlin.ExperimentalUnsignedTypes")
            optIn("kotlin.ExperimentalStdlibApi")
            optIn("kotlin.time.ExperimentalTime")
            optIn("kotlin.io.path.ExperimentalPathApi")
        }
    }
}

testing {
    suites {
        val test by getting(JvmTestSuite::class)
        val integrationTest by registering(JvmTestSuite::class) {
            testType = TestSuiteType.INTEGRATION_TEST

            sources {
                compileClasspath += test.sources.output
                runtimeClasspath += test.sources.output
            }

            dependencies {
                implementation(project())
            }

            targets {
                all {
                    testTask.configure {
                        systemProperty("cucumber.junit-platform.naming-strategy", "long")
                        shouldRunAfter(test)
                    }
                }
            }
        }

        withType<JvmTestSuite> {
            useJUnitJupiter()
            targets {
                all {
                    testTask.configure {
                        testLogging {
                            exceptionFormat = FULL
                            showStandardStreams = true
                            events("skipped", "failed")
                        }
                    }
                }
            }
        }
    }
}

val integrationTestImplementation by configurations.getting {
    extendsFrom(configurations.testImplementation.get())
}
val integrationTestRuntimeOnly by configurations.getting {
    extendsFrom(configurations.testRuntimeOnly.get())
}

configurations.all {
    exclude(group = "org.springframework.boot", module = "spring-boot-starter-logging")
    exclude(group = "io.projectreactor.netty", module = "reactor-netty-http-brave")
}

configurations.matching { it.name == CONFIGURATION_DETEKT }.all {
    resolutionStrategy.eachDependency {
        if (requested.group == "org.jetbrains.kotlin") {
            useVersion(getSupportedKotlinVersion())
        }
    }
}

dependencies {
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect"))

    implementation("com.fasterxml.jackson.module:jackson-module-kotlin")
    implementation("org.springframework.boot:spring-boot-starter-web")
    implementation("org.springframework.boot:spring-boot-starter-aop")
    implementation("org.springframework.data:spring-data-jdbc")
    implementation("org.springframework.boot:spring-boot-starter-log4j2")
    implementation("org.springframework.boot:spring-boot-starter-actuator")
    implementation("io.micrometer:micrometer-core")
    implementation("io.micrometer:micrometer-registry-otlp")
    implementation("io.micrometer:micrometer-tracing")
    implementation("io.micrometer:micrometer-tracing-bridge-otel")
    implementation("io.opentelemetry:opentelemetry-exporter-otlp")
    implementation("io.opentelemetry:opentelemetry-sdk-extension-autoconfigure")
    runtimeOnly(libs.otel.resources)
    implementation(libs.jdbc.micrometer)
    implementation(libs.mariadb.jdbc)
    implementation("com.zaxxer:HikariCP")
    implementation("org.flywaydb:flyway-mysql")
    implementation(libs.geolatte)
    implementation(libs.jooq)
    implementation(libs.jooq.kotlin)

    implementation(libs.pyroscope)

    implementation(libs.kotlin.logging)
    implementation(libs.slack.appender)
    implementation(libs.httpclient.slack.client)

    testImplementation("org.springframework.boot:spring-boot-starter-test")
    testImplementation("org.junit.jupiter:junit-jupiter-api")
    testImplementation("org.junit.jupiter:junit-jupiter-params")
    testImplementation("org.assertj:assertj-core")
    testImplementation("org.awaitility:awaitility")
    testImplementation("org.apache.commons:commons-lang3")
    testImplementation(libs.wiremock)
    testImplementation("org.mockito:mockito-junit-jupiter")
    testImplementation("org.mockito:mockito-core")
    testImplementation(rootProject.libs.mockito.kotlin)
    testImplementation(kotlin("test-junit5"))

    testImplementation(platform(libs.testcontainers.bom))
    testImplementation("org.testcontainers:mariadb")

    integrationTestImplementation(platform(libs.cucumber))
    integrationTestImplementation(libs.junit.suite.api)
    integrationTestImplementation("io.cucumber:cucumber-java8")
    integrationTestImplementation("io.cucumber:cucumber-java")
    integrationTestImplementation("io.cucumber:cucumber-spring")

    integrationTestRuntimeOnly(libs.junit.suite)
    integrationTestRuntimeOnly("io.cucumber:cucumber-junit-platform-engine")
}

versionCatalogUpdate {
    keep {
        libraries.add(libs.jooq.codegen)
    }
}

spotless {
    kotlin {
        ktlint().editorConfigOverride(
            mapOf(
                "ktlint_standard_no-wildcard-imports" to "disabled",
                "ktlint_standard_max-line-length" to "disabled",
            ),
        )
        targetExclude(fileTree(jooqDir))
    }
    kotlinGradle {
        ktlint()
    }
}

val downloadDetektConfig by tasks.registering(Download::class) {
    src("https://gitlab.com/bmorris591/detekt/-/raw/main/detekt.yaml?inline=false")
    dest(layout.buildDirectory.file("detekt.config"))
    onlyIfModified(true)
    useETag("all")
}

detekt {
    buildUponDefaultConfig = true
    allRules = true
    config.setFrom(files(downloadDetektConfig.get().dest))
}

tasks["check"].dependsOn("detektMain")

tasks.withType<Detekt> {
    jvmTarget = "21"
    dependsOn("downloadDetektConfig")
    exclude("**uk/co/borismorris/volvooncall/generated/**")
    dependsOn(jooqCodegen)
}

application {
    mainClass = mainClassKt
    applicationDefaultJvmArgs =
        listOfNotNull(
            "-XX:+DisableAttachMechanism",
            "-Dcom.sun.management.jmxremote",
            "-Dcom.sun.management.jmxremote.port=9000",
            "-Dcom.sun.management.jmxremote.local.only=false",
            "-Dcom.sun.management.jmxremote.authenticate=false",
            "-Dcom.sun.management.jmxremote.ssl=false",
            "-Dcom.sun.management.jmxremote.rmi.port=9000",
            "-Djava.rmi.server.hostname=127.0.0.1",
        )
}

jib {
    from {
        image = "azul/zulu-openjdk:21-jre-headless"
    }
    container {
        appRoot = "/opt/volvooncall"
        workingDirectory = "/opt/volvooncall"
        val javaToolOptions =
            listOfNotNull(
                "-XX:InitialRAMPercentage=50",
                "-XX:MaxRAMPercentage=85",
            ).joinToString(separator = " ")
        environment =
            mapOf(
                "JAVA_TOOL_OPTIONS" to javaToolOptions,
                "SPRING_PROFILES_ACTIVE" to "prod",
            )
        jvmFlags = listOfNotNull("-XX:+PrintCommandLineFlags", "-XX:+DisableExplicitGC")
        mainClass = mainClassKt
        creationTime = "USE_CURRENT_TIMESTAMP"
    }
}

@CacheableTask
abstract class JooqCodegenTask : DefaultTask() {
    @get:InputDirectory
    @get:PathSensitive(PathSensitivity.RELATIVE)
    abstract val migrations: DirectoryProperty

    @get:Input
    abstract val packageName: Property<String>

    @get:OutputDirectory
    abstract val outputDirectory: DirectoryProperty

    @TaskAction
    fun jooqCodegen() {
        withMariadbContainer { mariadbContainer ->
            val flyway =
                Flyway.configure()
                    .dataSource(mariadbContainer.jdbcUrl, mariadbContainer.username, mariadbContainer.password)
                    .locations("filesystem:${migrations.get().asFile.absolutePath}")
                    .load()
            flyway.migrate()

            GenerationTool.generate(
                Configuration()
                    .withJdbc(
                        Jdbc()
                            .withUrl(mariadbContainer.jdbcUrl)
                            .withUser(mariadbContainer.username)
                            .withPassword(mariadbContainer.password),
                    )
                    .withGenerator(
                        Generator()
                            .withName("org.jooq.codegen.KotlinGenerator")
                            .withDatabase(
                                Database()
                                    .withInputSchema(mariadbContainer.databaseName)
                                    .withOutputSchemaToDefault(true)
                                    .withExcludes(flyway.configuration.table)
                                    .withForcedTypes(
                                        listOf(
                                            ForcedType()
                                                .withUserType("uk.co.borismorris.volvooncall.jooq.bind.G2DPoint")
                                                .withBinding("uk.co.borismorris.volvooncall.jooq.bind.PointBinding")
                                                .withIncludeTypes("POINT"),
                                            ForcedType()
                                                .withUserType("uk.co.borismorris.volvooncall.jooq.bind.G2DPolygon")
                                                .withBinding("uk.co.borismorris.volvooncall.jooq.bind.PolygonBinding")
                                                .withIncludeTypes("POLYGON"),
                                        ),
                                    ),
                            )
                            .withGenerate(
                                Generate()
                                    .withImplicitJoinPathsAsKotlinProperties(true)
                                    .withKotlinSetterJvmNameAnnotationsOnIsPrefix(true)
                                    .withRecords(false),
                            )
                            .withTarget(
                                Target()
                                    .withPackageName(packageName.get())
                                    .withDirectory(outputDirectory.asFile.get().absolutePath),
                            ),
                    ),
            )
        }
    }

    private fun withMariadbContainer(withContainer: (JdbcDatabaseContainer<*>) -> Unit) {
        val mariadbContainer: JdbcDatabaseContainer<*> =
            MariaDBContainerProvider().newInstance("latest")
                .withUsername("migrate")
                .withPassword("migrate")
                .withDatabaseName("migrate")
        mariadbContainer.start()
        mariadbContainer.use(withContainer)
    }
}
